package iolesson;

import com.outlook.volodimir.oracleacademy.jprogramming.lessons.io.FileProcessor;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by volodimir on 21.02.17.
 */
public class FileProcessorTest {

    @Test
    public void testFillTextFileWithRandom() throws IOException {
        new FileProcessor().fillTextFileWithRandom("./ioLesson/RandomNumbers", 1000);
    }

    @Test
    public void testSortNumbersOfTextFile() throws IOException {
        new FileProcessor().sortNumbersOfTextFile("./ioLesson/RandomNumbers");
    }

    @Test
    public void testPrintMeanMarksAbove() throws IOException {
        //sadewf ewb=90
        //dfg rturtur=90
        new FileProcessor().printMeanMarksAbove(90, "ioLesson/studentsMarks");
    }

    @Test
    public void testReplaceFirstAndLastInLine() throws IOException {
        new FileProcessor().replaceFirstAndLastInLine("ioLesson/Malien'kii ubiitsa",
                "ioLesson/Malien'kii ubiitsa - replaced words in lines");
    }

    @Test
    public void testReplaceFirstAndLastInSentence() throws IOException {
        new FileProcessor().replaceFirstAndLastInSentence("ioLesson/Malien'kii ubiitsa",
                "ioLesson/Malien'kii ubiitsa - replaced words in sentences");
    }

    @Test
    public void testCompareCopyTime() throws IOException {
        new FileProcessor().compareCopyTime("ioLesson/Rouge_39x27_CMYK-1.jpg",
                "ioLesson/copy Rouge_39x27_CMYK-1.jpg");
    }
}
