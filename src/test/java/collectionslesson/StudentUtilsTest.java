package collectionslesson;

import com.outlook.volodimir.oracleacademy.jprogramming.lessons.collections.Student;
import com.outlook.volodimir.oracleacademy.jprogramming.lessons.collections.StudentUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by volodimir on 16.02.17.
 */
public class StudentUtilsTest {

    private List<Student> studentsList;

    @Before
    public void prepareListOfStudents() {
        studentsList = new ArrayList<>(5);
        studentsList.add(0, new Student("John", "Kennedy", 1));
        studentsList.add(1, new Student("George", "Bush", 2));
        studentsList.add(2, new Student("Abraham", "Lincoln", 3));
        studentsList.add(3, new Student("Thomas", "Wilson", 4));
        studentsList.add(4, new Student("George", "Washington", 5));
    }

    @Test
    public void testCreateMapFromList() {
        Map<String, Student> map = StudentUtils.createMapFromList(studentsList);
        boolean condition0 =  map.get("John Kennedy").equals(studentsList.get(0));
        boolean condition1 =  map.get("George Bush").equals(studentsList.get(1));
        boolean condition2 =  map.get("Abraham Lincoln").equals(studentsList.get(2));
        boolean condition3 =  map.get("Thomas Wilson").equals(studentsList.get(3));
        boolean condition4 =  map.get("George Washington").equals(studentsList.get(4));
        Assert.assertTrue(condition0 && condition1 && condition2 && condition3 && condition4);
    }

    @Test
    public void testPrintStudents() {
        StudentUtils.printStudents(studentsList, 4); //"Thomas"
    }

    @Test
    public void testSortStudent() {
        List<Student> sortedList = StudentUtils.sortStudent(studentsList);
        boolean condition0 =  sortedList.get(3).equals(studentsList.get(0));
        boolean condition1 =  sortedList.get(1).equals(studentsList.get(1)) ||
                sortedList.get(2).equals(studentsList.get(1));
        boolean condition2 =  sortedList.get(0).equals(studentsList.get(2));
        boolean condition3 =  sortedList.get(4).equals(studentsList.get(3));
        boolean condition4 =  sortedList.get(1).equals(studentsList.get(4)) ||
                sortedList.get(2).equals(studentsList.get(4));
        Assert.assertTrue(condition0 && condition1 && condition2 && condition3 && condition4);
    }
}
