package stringproccessing;

import com.outlook.volodimir.oracleacademy.jprogramming.homeworks.stringprocessing.StringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by volodimir on 29.01.17.
 */
public class StringUtilsTest {
    @Test
    public void testTurnOverString() {
        String actual = StringUtils.turnOverString("Hello world!");
        String expected ="!dlrow olleH";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIsPalindrome() {
        boolean condition1 = StringUtils.isPalindrome("Was it a car or a cat I saw");
        boolean condition2 = !StringUtils.isPalindrome("Was it a cat or a car I saw");
        Assert.assertTrue(condition1);
        Assert.assertTrue(condition2);
    }

    @Test
    public void testTrimOrExtendString() {
        String actual = StringUtils.trimOrExtendString("Was it a car or a cat I saw");
        String expected ="Was it";
        Assert.assertEquals(expected, actual);
        actual = StringUtils.trimOrExtendString("Was it a ");
        expected ="Was it a ooo";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testExchangeFirstAndLastWords() {
        String actual = StringUtils.exchangeFirstAndLastWords("Was it a car or a cat I saw");
        String expected ="saw it a car or a cat I Was";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testExchangeFirstAndLastWordsOfSentence() {
        String actual = StringUtils.exchangeFirstAndLastWordsOfSentence("The most familiar palindromes in English " +
                "are character-unit palindromes. The characters read the same backward as forward. " +
                "Some examples of palindromic words are redivider, noon, civic, radar, level, rotor, " +
                "kayak, reviver, racecar, redder, madam, and refer. ");
        String expected ="palindromes most familiar palindromes in English are character-unit The. " +
                "forward characters read the same backward as The. refer examples of palindromic " +
                "words are redivider, noon, civic, radar, level, rotor, kayak, reviver, " +
                "racecar, redder, madam, and Some. ";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIsOnlyabc() {
        boolean condition1 = StringUtils.isOnlyabc("aaaabbbccc");
        boolean condition2 = !StringUtils.isOnlyabc("Hello world!");
        Assert.assertTrue(condition1);
        Assert.assertTrue(condition2);
    }

    @Test
    public void testIsDateMMpDDpYYYY() {
        boolean condition1 = StringUtils.isDateMMpDDpYYYY("01.25.1995");
        boolean condition2 = !StringUtils.isDateMMpDDpYYYY("02.256.2008");
        Assert.assertTrue(condition1);
        Assert.assertTrue(condition2);
    }

    @Test
    public void testIsEmail() {
        boolean condition1 = StringUtils.isEmail("login@mail.fm");
        boolean condition2 = !StringUtils.isEmail("login@$mail.fm");
        Assert.assertTrue(condition1);
        Assert.assertTrue(condition2);
    }

    @Test
    public void testExtractTelephoneNumbers() {
        String[] actual = StringUtils.extractTelephoneNumbers("Telephone numbers: +8(076)255-74-89, 0942149753, " +
                "+2(476)885-14-56 and 789-24-15.");
        String[] expected = new String[] {"+8(076)255-74-89", "+2(476)885-14-56"};
        Assert.assertArrayEquals(expected, actual);
    }
}
