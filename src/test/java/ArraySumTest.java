import com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting.ArraySum;
import org.junit.*;

/**
 * Created by volodimir on 24.01.17.
 */
public class ArraySumTest {

    private static int[] testArray;

    @BeforeClass
    public static void prepareTestArray() {
        ArraySumTest.testArray = new int[]{1, 2, 3, 4, 5};
    }

    @AfterClass
    public static void destroyTestArray() {
        ArraySumTest.testArray = null;
    }

    @Test
    public void testSumOfTheObjectArray() {
        int expected = 15;
        ArraySum arrSum = new ArraySum(ArraySumTest.testArray);
        int actual = arrSum.sum();
        Assert.assertEquals("Test of 'sum()' did not pass", expected, actual);

    }

    @Test
    public void testSumOfArrayStatic() {
        int expected = 15;
        int actual = ArraySum.sum(ArraySumTest.testArray);
        Assert.assertEquals("Test of 'static sum(int[] array)' did not pass", expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void testSumOfArrayStaticWithNull() {
        ArraySum.sum(null);
    }
}
