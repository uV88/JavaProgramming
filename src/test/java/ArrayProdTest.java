import com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting.ArrayProd;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by volodimir on 25.01.17.
 */
public class ArrayProdTest {
    private static int[] testArray;

    @BeforeClass
    public static void prepareTestArray() {
        ArrayProdTest.testArray = new int[]{1, 2, 3, 4, 5};
    }

    @AfterClass
    public static void destroyTestArray() {
        ArrayProdTest.testArray = null;
    }

    @Test
    public void testProdOfTheObjectArray() {
        int expected = 120;
        ArrayProd arrProd = new ArrayProd(ArrayProdTest.testArray);
        int actual = arrProd.product();
        Assert.assertEquals("Test of 'product()' did not pass", expected, actual);

    }

    @Test
    public void testProdOfArrayStatic() {
        int expected = 120;
        int actual = ArrayProd.product(ArrayProdTest.testArray);
        Assert.assertEquals("Test of 'static product(int[] array)' did not pass", expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void testProdOfArrayStaticWithNull() {
        ArrayProd.product(null);
    }
}
