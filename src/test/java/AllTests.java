import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by volodimir on 25.01.17.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ArraySumTest.class, ArrayProdTest.class
})
public class AllTests {
}
