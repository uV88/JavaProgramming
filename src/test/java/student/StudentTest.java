package student;

import com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting.student.Group;
import com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting.student.PassedExamination;
import com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting.student.Student;
import org.junit.*;

/**
 * Created by volodimir on 25.01.17.
 */
public class StudentTest {

    private static PassedExamination[] passedExaminations;
    private Group group;
    private Student student;

    @BeforeClass
    public static void preparePassedExaminationsList() {
        StudentTest.passedExaminations = new PassedExamination[5];
        StudentTest.passedExaminations[0] = new PassedExamination("Mathematic", 5, 2010, 1);
        StudentTest.passedExaminations[1] = new PassedExamination("Mathematic", 4, 2010, 2);
        StudentTest.passedExaminations[2] = new PassedExamination("Physics", 5, 2010, 2);
        StudentTest.passedExaminations[3] = new PassedExamination("Physics", 4, 2011, 1);
        StudentTest.passedExaminations[4] = new PassedExamination("Chemistry", 3, 2010, 2);
    }

    @AfterClass
    public static void clearPassedExaminationsList() {
        StudentTest.passedExaminations = null;
    }

    @Before
    public void prepareStudentOfGroup() {
        this.group = new Group(2011, "Physical-Technical");
        this.student = new Student("Viktor", "", group, 60);
    }

    @After
    public void deleteStudentOfGroup() {
        this.group = null;
        this.student = null;
    }

    @Test
    public void testExaminationPassingAndDeleting() {

        boolean condition1 = this.student.passExamination(StudentTest.passedExaminations[0]);
        this.student.deleteFromPassed("Mathematic", 2010, 1);
        boolean condition2 = true;
        Assert.assertTrue(condition1);
        Assert.assertTrue(condition2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExaminationDeletingWithException() {
        this.student.deleteFromPassed("Mathematic", 2010, 1);
    }

    @Test
    public void testExaminationPassingAndgMaxMarkIn() {
        boolean condition1 = this.student.passExamination(StudentTest.passedExaminations[0]);
        boolean condition2 = this.student.passExamination(StudentTest.passedExaminations[1]);
        boolean condition3 = this.student.passExamination(StudentTest.passedExaminations[2]);
        int expected = 5;
        int actual = this.student.maxMarkIn("Mathematic");
        Assert.assertTrue(condition1);
        Assert.assertTrue(condition2);
        Assert.assertTrue(condition3);
        Assert.assertEquals("Test of 'maxMarkIn(String subject)' did not pass", expected, actual);
    }

    @Test
    public void testExaminationPassingAndNumOfExamWith() {
        boolean condition1 = this.student.passExamination(StudentTest.passedExaminations[0]);
        boolean condition2 = this.student.passExamination(StudentTest.passedExaminations[1]);
        boolean condition3 = this.student.passExamination(StudentTest.passedExaminations[2]);
        int expected = 2;
        int actual = this.student.numOfExaminationsWith(5);
        Assert.assertTrue(condition1);
        Assert.assertTrue(condition2);
        Assert.assertTrue(condition3);
        Assert.assertEquals("Test of 'numOfExaminationsWith(int mark)' did not pass", expected, actual);
    }

    @Test
    public void testExaminationPassingAndMeanMarkOf() {
        boolean condition1 = this.student.passExamination(StudentTest.passedExaminations[0]);
        boolean condition2 = this.student.passExamination(StudentTest.passedExaminations[1]);
        boolean condition3 = this.student.passExamination(StudentTest.passedExaminations[2]);
        boolean condition4 = this.student.passExamination(StudentTest.passedExaminations[3]);
        boolean condition5 = this.student.passExamination(StudentTest.passedExaminations[4]);
        int expected = 4;
        int actual = this.student.meanMarkOf(2010, 2);
        Assert.assertTrue(condition1);
        Assert.assertTrue(condition2);
        Assert.assertTrue(condition3);
        Assert.assertTrue(condition4);
        Assert.assertTrue(condition5);
        Assert.assertEquals("Test of 'meanMarkOf(int year, int semester)' did not pass", expected, actual);
    }

}
