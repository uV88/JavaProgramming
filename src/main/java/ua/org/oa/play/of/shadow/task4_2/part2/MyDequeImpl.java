package ua.org.oa.play.of.shadow.task4_2.part2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by volodimir on 13.02.17.
 */
public class MyDequeImpl<E> implements MyDeque<E> {

    private Node<E> header = new Node<>();
    {
        header.next = header;
        header.prev = header;
    }
    private int nodesCount = 0;

    /**
     * Add new element at the front of this deque
     * @param e - the element to add
     */
    @Override
    public void addFirst(E e) {
        add(e, header.next, header);
    }

    /**
     * Add new element at the end of this deque
     * @param e - the element to add
     */
    @Override
    public void addLast(E e) {
        add(e, header, header.prev);
    }

    private void add(E e, Node<E>refToTheNext, Node<E>refToThePrev) {
        Node<E> newNode = new Node<>(e, refToTheNext, refToThePrev);
        newNode.next.prev = newNode;
        newNode.prev.next = newNode;
        nodesCount++;
    }

    /**
     * Retrieves and removes the first element of this deque
     * @return the head of this deque
     */
    @Override
    public E removeFirst() {
        return remove(header.next);
    }

    /**
     * Retrieves and removes the last element of this deque
     * @return the tail of this deque
     */
    @Override
    public E removeLast() {
        return remove(header.prev);
    }

    private E remove(Node<E> removedNode) {
        removedNode.next.prev = removedNode.prev;
        removedNode.prev.next = removedNode.next;
        nodesCount--;
        return removedNode.element;
    }

    /**
     * Retrieves, but does not remove, the first element of this deque
     * @return the head of this deque
     */
    @Override
    public E getFirst(){
        return header.next.element;
    }

    /**
     * Retrieves, but does not remove, the last element of this deque
     * @return the tail of this deque
     */
    @Override
    public E getLast() {
        return header.prev.element;
    }

    /**
     * Returns true if this deque contains element which is equivalent to the specified object
     * @param o - specified object
     * @return true if this deque contains element which is equivalent to the specified object
     */
    @Override
    public boolean contains(Object o) {
        Node<E> node = header.next;
        while(node != header) {
            if(node.element.equals(o)) {
                return true;
            }
            node = node.next;
        }
        return false;
    }

    /**
     * Removes all of the elements from this deque
     */
    @Override
    public void clear() {
        header.next = header;
        header.prev = header;
        nodesCount = 0;
    }

    /**
     * Returns an array containing all of the elements in this deque
     * @return an array containing all of the elements in this deque
     */
    @Override
    public Object[] toArray() {
        Object[] elements = new Object[nodesCount];
        Node<E> node = header.next;
        for(int i = 0; i < elements.length; i++) {
            elements[i] = node.element;
            node = node.next;
        }
        return elements;
    }

    /**
     * Returns the number of elements in this deque
     * @return the number of elements in this deque
     */
    @Override
    public int size() {
        return nodesCount;
    }

    /**
     * Returns true if this deque contains elements which are equivalent to all of the elements in the specified deque
     * @param deque - deque to be checked for containment in this deque
     * @return true if this deque contains elements which are equivalent to all of the elements in the specified deque
     */
    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        for(E element : deque) {
            if(!contains(element)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns an iterator over elements of type E
     * @return an Iterator
     */
    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("MyDequeImpl{");
        Node<E> node = header.next;
        while(node != header) {
            stringBuilder.append(node.element);
            stringBuilder.append(", ");
            node = node.next;
        }
        return stringBuilder.append("}").toString();
    }

    /**
     * Static nested class Node
     * @param <E>
     */
    private static class Node<E> {

        E element;
        Node<E> next;
        Node<E> prev;

        /**
         * Creates new Node object
         */
        Node() {
        }

        /**
         * Creates new Node object
         * @param element - element this node contains
         * @param next - reference to the next Node in the deque
         * @param prev - reference to the previous Node in the deque
         */
        Node(E element, Node<E> next, Node<E> prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }
    }

    /**
     * Inner class IteratorImpl
     */
    private class IteratorImpl implements Iterator<E> {

        Node<E> nodeAtCursor = header;
        Node<E> deletedNode = header;

        /**
         * Returns true if the iteration has more elements.
         * (In other words, returns true if next() would return an element rather than throwing an exception.)
         * @return true if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return nodeAtCursor.next != header;
        }

        /**
         * Returns the next element in the iteration.
         * @return the next element in the iteration
         */
        @Override
        public E next() {
            nodeAtCursor = nodeAtCursor.next;
            if(nodeAtCursor == header) throw new NoSuchElementException();
            return nodeAtCursor.element;
        }

        /**
         * Removes from the underlying collection the last element returned by this iterator.
         * This method can be called only once per call to next().
         */
        @Override
        public void remove() {
            if(nodeAtCursor == deletedNode) throw new IllegalStateException();
            nodeAtCursor.prev.next = nodeAtCursor.next;
            nodeAtCursor.next.prev = nodeAtCursor.prev;
            deletedNode = nodeAtCursor;
        }
    }
}
