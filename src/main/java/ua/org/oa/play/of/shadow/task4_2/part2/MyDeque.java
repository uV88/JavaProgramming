package ua.org.oa.play.of.shadow.task4_2.part2;

/**
 * Created by volodimir on 13.02.17.
 */
public interface MyDeque<E> extends Iterable<E> {

    /**
     * Add new element at the front of this deque
     * @param e - the element to add
     */
    void addFirst(E e);

    /**
     * Add new element at the end of this deque
     * @param e - the element to add
     */
    void addLast(E e);

    /**
     * Retrieves and removes the first element of this deque
     * @return the head of this deque
     */
    E removeFirst();

    /**
     * Retrieves and removes the last element of this deque
     * @return the tail of this deque
     */
    E removeLast();

    /**
     * Retrieves, but does not remove, the first element of this deque
     * @return the head of this deque
     */
    E getFirst();

    /**
     * Retrieves, but does not remove, the last element of this deque
     * @return the tail of this deque
     */
    E getLast();

    /**
     * Returns true if this deque contains element which is equivalent to the specified object
     * @param o - specified object
     * @return true if this deque contains element which is equivalent to the specified object
     */
    boolean contains(Object o);

    /**
     * Removes all of the elements from this deque
     */
    void clear();

    /**
     * Returns an array containing all of the elements in this deque
     * @return an array containing all of the elements in this deque
     */
    Object[] toArray();

    /**
     * Returns the number of elements in this deque
     * @return the number of elements in this deque
     */
    int size();

    /**
     * Returns true if this deque contains elements which are equivalent to all of the elements in the specified deque
     * @param deque - deque to be checked for containment in this deque
     * @return true if this deque contains elements which are equivalent to all of the elements in the specified deque
     */
    boolean containsAll(MyDeque<? extends E> deque);
}
