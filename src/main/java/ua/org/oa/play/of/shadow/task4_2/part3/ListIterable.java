package ua.org.oa.play.of.shadow.task4_2.part3;

/**
 * Created by volodimir on 17.02.17.
 */
public interface ListIterable<E> {

    /**
     * Returns a list iterator over elements of type E
     * @return a ListIterator
     */
    ListIterator<E> listIterator();

}
