package ua.org.oa.play.of.shadow.task4_1;

/**
 * Created by volodimir on 12.02.17.
 */
public class MaxValueFinder {

    public static <T extends Comparable<T>> T findMaxValue(T[] values) {
        T maxValue = values[0];
        for(T value : values) {
            if(value.compareTo(maxValue) > 0) {
                maxValue = value;
            }
        }
        return maxValue;
    }

    public static void main(String[] args) {
        Integer[] integers = new Integer[]{2, 5, 1, 8, -2, 4, -100};
        System.out.println("Max Integer: " + MaxValueFinder.findMaxValue(integers));

        String[] strings = new String[]{"d", "cat", "car", "a", "finder", "String", "boolean"};
        System.out.println("Max String: " + MaxValueFinder.findMaxValue(strings));

        Car[] cars = new Car[]{new Car(4, 2, 200), new Car(6, 4, 120), new Car(4, 4, 150)};
        //inferred type does not conform to upper bound(s):
        //System.out.println("Max Car: " + MaxValueFinder.findMaxValue(cars));

        Computer[] computers = new Computer[]{new Computer(2_500, 4, 4_000), new Computer(3_000, 2, 8_000), new Computer(6)};
        System.out.println("Max Computer: " + MaxValueFinder.findMaxValue(computers));
    }
}
