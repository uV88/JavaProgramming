package ua.org.oa.play.of.shadow.task4_2.part1;

import java.util.Arrays;

/**
 * Created by volodimir on 12.02.17.
 */
public class Demo {

    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        System.out.println("----------------------------------------------------");
        deque.addFirst(20);
        deque.addLast(25.5);
        deque.addFirst(18.7);
        deque.addLast(37);
        System.out.println(deque.toString() + " -> size: " + deque.size());
        System.out.println("Removes first " + deque.removeFirst());
        System.out.println("Removes first " + deque.removeFirst());
        System.out.println("Removes last " + deque.removeLast());
        System.out.println(deque.toString() + " -> size: " + deque.size());
        System.out.println("----------------------------------------------------");
        deque.addFirst(200);
        deque.addFirst(300.111);
        deque.addFirst(400.2);
        deque.addLast(24);
        deque.addLast(23.5f);
        deque.addLast(22);
        System.out.println(deque.toString() + " -> size: " + deque.size());
        System.out.println("Gets first " + deque.getFirst());
        System.out.println("Gets first " + deque.getFirst());
        System.out.println("Gets last " + deque.getLast());
        System.out.println(deque.toString() + " -> size: " + deque.size());
        System.out.println("Contains 300.111 " + deque.contains(300.111));
        System.out.println("Contains 10 " + deque.contains(10));
        System.out.println("Contains 25.5 " + deque.contains(25.5));
        System.out.println("----------------------------------------------------");
        deque.clear();
        System.out.println(deque.toString() + " -> size: " + deque.size());
        System.out.println("'toArray()' -> " + Arrays.toString(deque.toArray()));
        System.out.println("Contains 25 " + deque.contains(25));
        System.out.println("----------------------------------------------------");
        deque.addFirst(7);
        deque.addLast(8);
        deque.addFirst(6);
        deque.addLast(9);
        System.out.println(deque.toString() + " -> size: " + deque.size());
        System.out.println("'toArray()' -> " + Arrays.toString(deque.toArray()));
        System.out.println("----------------------------------------------------");
        MyDeque<Integer> anotherDeque = new MyDequeImpl<>();
        anotherDeque.addLast(9);
        anotherDeque.addLast(6);
        System.out.println("another deque:");
        System.out.println(anotherDeque.toString() + " -> size: " + anotherDeque.size());
        System.out.println("Contains another deque " + deque.containsAll(anotherDeque));
        System.out.println("----------------------------------------------------");
        anotherDeque.clear();
        anotherDeque.addLast(0);
        anotherDeque.addLast(100500);
        System.out.println("another deque:");
        System.out.println(anotherDeque.toString() + " -> size: " + anotherDeque.size());
        System.out.println("Contains another deque " + deque.containsAll(anotherDeque));
        System.out.println("----------------------------------------------------");
        anotherDeque.clear();
        anotherDeque.addLast(9);
        anotherDeque.addLast(6);
        anotherDeque.addLast(5);
        System.out.println("another deque:");
        System.out.println(anotherDeque.toString() + " -> size: " + anotherDeque.size());
        System.out.println("Contains another deque " + deque.containsAll(anotherDeque));
        System.out.println("----------------------------------------------------");
    }
}
