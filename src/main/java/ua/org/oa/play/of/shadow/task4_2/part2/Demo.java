package ua.org.oa.play.of.shadow.task4_2.part2;

import java.util.Iterator;

/**
 * Created by volodimir on 13.02.17.
 */
public class Demo {

    public static void main(String[] args) {

        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(444);
        deque.addLast(8.88);
        deque.addFirst(555);
        deque.addLast(4.44);
        deque.addFirst(666);
        deque.addLast(2.22);

        Iterator<Number> it = deque.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        System.out.println("-------------------------------------------------");

        it = deque.iterator();
        if(it.hasNext()) it.next();
        it.remove(); //remove 666
        if(it.hasNext()) it.next();
        if(it.hasNext()) it.next();
        it.remove(); //remove 444
        if(it.hasNext()) it.next();
        if(it.hasNext()) it.next();
        if(it.hasNext()) it.next();
        it.remove(); //remove 2.22

        for (Number element : deque) {
            System.out.println(element);
        }

        //Exception in thread "main" java.util.NoSuchElementException:
        //it.next();
        System.out.println("-------------------------------------------------");

        //Exception in thread "main" java.lang.IllegalStateException:
        //it = deque.iterator();
        //it.remove();

        //Exception in thread "main" java.lang.IllegalStateException:
        //it = deque.iterator();
        //it.next();
        //it.remove();
        //it.remove();
    }
}
