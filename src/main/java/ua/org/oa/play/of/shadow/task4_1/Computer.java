package ua.org.oa.play.of.shadow.task4_1;

/**
 * Created by volodimir on 12.02.17.
 */
public class Computer implements Comparable<Computer> {

    private int processorClockSpeed;
    private final int processorCoresNumber;
    private int sizeOfRAM;

    /**
     * Creates new Computer object
     * @param processorCoresNumber - number of processor cores of computer
     */
    public Computer(int processorCoresNumber) {
        this.processorCoresNumber = processorCoresNumber;
    }

    /**
     * Creates new Computer object
     * @param processorClockSpeed - processor clock speed
     * @param processorCoresNumber - number of processor cores of computer
     * @param sizeOfRAM - RAM size
     */
    public Computer(int processorClockSpeed, int processorCoresNumber, int sizeOfRAM) {
        this.processorClockSpeed = processorClockSpeed;
        this.processorCoresNumber = processorCoresNumber;
        this.sizeOfRAM = sizeOfRAM;
    }

    public int getProcessorClockSpeed() {
        return processorClockSpeed;
    }

    public void setProcessorClockSpeed(int processorClockSpeed) {
        this.processorClockSpeed = processorClockSpeed;
    }

    public int getProcessorCoresNumber() {
        return processorCoresNumber;
    }

    public int getSizeOfRAM() {
        return sizeOfRAM;
    }

    public void setSizeOfRAM(int sizeOfRAM) {
        this.sizeOfRAM = sizeOfRAM;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processorClockSpeed=" + processorClockSpeed +
                ", processorCoresNumber=" + processorCoresNumber +
                ", sizeOfRAM=" + sizeOfRAM +
                '}';
    }

    @Override
    public int compareTo(Computer computer) {
        int difference = processorClockSpeed * processorCoresNumber -
                computer.processorClockSpeed * computer.processorCoresNumber;
        return (difference == 0) ? (sizeOfRAM - computer.sizeOfRAM) : difference;
    }
}
