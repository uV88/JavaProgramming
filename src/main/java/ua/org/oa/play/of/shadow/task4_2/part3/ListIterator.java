package ua.org.oa.play.of.shadow.task4_2.part3;

import java.util.Iterator;

/**
 * Created by volodimir on 17.02.17.
 */
public interface ListIterator<E> extends Iterator<E> {

    /**
     * Returns true if this list iterator has more elements when traversing the list in the reverse direction.
     * (In other words, returns true if previous() would return an element rather than throwing an exception.)
     * @return true if the list iterator has more elements when traversing the list in the reverse direction
     */
    boolean hasPrevious();

    /**
     * Returns the previous element in the list and moves the cursor position backwards.
     * This method may be called repeatedly to iterate through the list backwards,
     * or intermixed with calls to next() to go back and forth.
     * (Note that alternating calls to next and previous will return the same element repeatedly.)
     * @return the previous element in the list
     */
    E previous();

    /**
     * Replaces the last element returned by next() or previous() with the specified element.
     * This call can be made only if remove() have not been called after the last call to next or previous.
     * @param e - the element to insert
     */
    void set(E e);

    /**
     * Removes from the list the last element that was returned by next() or previous().
     * This call can only be made once per call to next or previous.
     */
    void remove();
}
