package ua.org.oa.play.of.shadow.task4_1;

/**
 * Created by volodimir on 12.02.17.
 */
public class Car {

    private int wheelNumber;
    private int drivingWheelNumber;
    private int maxSpeed;

    /**
     * Creates new Car object
     */
    public Car() {
    }

    /**
     * Creates new Car object
     * @param wheelNumber - number of wheels
     * @param drivingWheelNumber - number of driving wheels
     * @param maxSpeed - maximal speed
     */
    public Car(int wheelNumber, int drivingWheelNumber, int maxSpeed) {
        this.wheelNumber = wheelNumber;
        this.drivingWheelNumber = drivingWheelNumber;
        this.maxSpeed = maxSpeed;
    }

    public int getWheelNumber() {
        return wheelNumber;
    }

    public void setWheelNumber(int wheelNumber) {
        this.wheelNumber = wheelNumber;
    }

    public int getDrivingWheelNumber() {
        return drivingWheelNumber;
    }

    public void setDrivingWheelNumber(int drivingWheelNumber) {
        this.drivingWheelNumber = drivingWheelNumber;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "wheelNumber=" + wheelNumber +
                ", drivingWheelNumber=" + drivingWheelNumber +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}
