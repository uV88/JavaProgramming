package ua.org.oa.play.of.shadow.task4_2.part3;

/**
 * Created by volodimir on 17.02.17.
 */
public class Demo {

    public static void main(String[] args) {

        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(444);
        deque.addLast(8.88);
        deque.addFirst(555);
        deque.addLast(4.44);
        deque.addFirst(666);
        deque.addLast(2.22);
        ListIterator<Number> lIt = deque.listIterator();

        //Exception in thread "main" java.lang.IllegalStateException:
        //lIt.remove();
        //Exception in thread "main" java.lang.IllegalStateException:
        //lIt.set(100500);

        while (lIt.hasNext()) {
            System.out.println(lIt.next());
        }

        //Exception in thread "main" java.util.NoSuchElementException:
        //lIt.next();
        System.out.println("-------------------------------------------------");

        while (lIt.hasPrevious()) {
            System.out.println(lIt.previous());
        }
        //Exception in thread "main" java.util.NoSuchElementException:
        //lIt.previous();
        System.out.println("-------------------------------------------------");

        if (lIt.hasNext()) {
            System.out.println(lIt.next()); //666
        }
        if (lIt.hasNext()) {
            System.out.println(lIt.next()); //555
        }
        if (lIt.hasNext()) {
            System.out.println(lIt.next()); //444
        }
        if (lIt.hasPrevious()) {
            System.out.println(lIt.previous()); //444
        }
        if (lIt.hasPrevious()) {
            System.out.println(lIt.previous()); //555
        }
        if (lIt.hasPrevious()) {
            System.out.println(lIt.previous()); //666
        }
        System.out.println("-------------------------------------------------");
        while (lIt.hasNext()) {
            lIt.next();
        }
        if (lIt.hasPrevious()) {
            System.out.println(lIt.previous()); //2.22
        }
        if (lIt.hasPrevious()) {
            System.out.println(lIt.previous()); //4.44
        }
        if (lIt.hasPrevious()) {
            System.out.println(lIt.previous()); //8.88
        }
        if (lIt.hasNext()) {
            System.out.println(lIt.next()); //8.88
        }
        if (lIt.hasNext()) {
            System.out.println(lIt.next()); //4.44
        }
        if (lIt.hasNext()) {
            System.out.println(lIt.next()); //2.22
        }
        System.out.println("-------------------------------------------------");

        if(lIt.hasPrevious()) lIt.previous();
        lIt.remove(); //remove 2.22
        if(lIt.hasPrevious()) lIt.previous();
        if(lIt.hasPrevious()) lIt.previous();
        lIt.remove(); //remove 8.88
        if(lIt.hasPrevious()) lIt.previous();
        if(lIt.hasPrevious()) lIt.previous();
        lIt.remove(); //remove 555
        if(lIt.hasPrevious()) lIt.previous();
        lIt.set(0); //666 -> 0
        if (lIt.hasNext()) lIt.next();
        if (lIt.hasNext()) lIt.next();
        lIt.set(1); //444 -> 1
        for (Number element : deque) {
            System.out.println(element);
        }
        System.out.println("-------------------------------------------------");

        //Exception in thread "main" java.lang.IllegalStateException:
        /*lIt = deque.listIterator();
          lIt.next();
          lIt.remove();
          lIt.remove();
        */
        //Exception in thread "main" java.lang.IllegalStateException:
        /*lIt = deque.listIterator();
          lIt.next();
          lIt.remove();
          lIt.set(100500);
        */
    }
}
