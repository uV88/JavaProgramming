package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.nestedclasses;

/**
 * Created by volodimir on 28.01.17.
 */
public class Car {

    private int maxSpeed;
    private int seatsCount;

    public Car(int maxSpeed, int seatsCount) {
        this.maxSpeed = maxSpeed;
        this.seatsCount = seatsCount;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getSeatsCount() {
        return seatsCount;
    }

    @Override
    public String toString() {
        return "abstract car: max speed " + maxSpeed + ", seats " + seatsCount;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if(obj != null) {
            if(obj.getClass() == this.getClass()) {
                Car car = (Car)obj;
                equals = (this.maxSpeed == car.maxSpeed && this.seatsCount == car.seatsCount);
            }
        }
        return equals;
    }

    public static void main(String[] args) {
        Car car = new Car(220, 4);
        Car electricCar = new Car(220, 4) {

            @Override
            public String toString() {
                return "electric car: max speed " + this.getMaxSpeed() + ", seats " + this.getSeatsCount();
            }
        };
        Car hybridCar = new Car(220, 4) {

            @Override
            public String toString() {
                return "hybrid car: max speed " + this.getMaxSpeed() + ", seats " + this.getSeatsCount();
            }

            @Override
            public boolean equals(Object obj) {
                boolean equals = false;
                if(obj != null) {
                    if(obj.getClass() == Car.class) {
                        Car car = (Car)obj;
                        equals = (this.getMaxSpeed() == car.getMaxSpeed() && this.getSeatsCount() == car.getSeatsCount());
                    }
                }
                return equals;
            }
        };
        System.out.println(electricCar + ", the same:" + electricCar.equals(car));
        System.out.println(hybridCar + ", the same:" + hybridCar.equals(car));
    }
}
