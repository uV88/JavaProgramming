package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.ioextended.filelist;

import java.io.IOException;
import java.util.List;

/**
 * Created by volodimir on 23.02.17.
 */
public interface FileListExplorer<T> {

    /**
     * Refreshes content of list of an object according to actual state of file list
     * @throws IOException - if an I/O error occurs
     */
    void refresh() throws IOException;

    /**
     * Adds new item to the list of an object
     * and new appropriate record to the associated file list
     * @param item - item
     * @throws IOException - if an I/O error occurs
     */
    void addItem(T item) throws IOException;

    /**
     * Returns list reflecting content of associated file
     * at the moment of refresh() method last call
     * @return list reflecting content of associated file
     */
    List<T> exportList();
}
