package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

/**
 * Created by volodimir on 07.02.17.
 */
public class CombinedStorage<V extends Entity<Integer>> implements GenericStorage<Integer, V> {

    public static final int STORAGE_RANGE = 10;

    private Pair<V>[] storage = (Pair<V>[]) new Pair[CombinedStorage.STORAGE_RANGE];
    private int newPairId = 0;

    /**
     * Adds new value to this storage
     * @param value - object for adding
     * @return key, associated with object
     */
    @Override
    public Integer add(V value) {
        Pair<V> newPair  = new Pair<>(newPairId, value);
        add(newPair);
        return newPairId++;
    }

    private void add(Pair<V> newPair) {
        int index = newPair.key % storage.length;
        if(storage[index] != null) {
            newPair.nextPair = storage[index];
        }
        storage[index] = newPair;
    }

    /**
     * Returns value by its key
     * @param key - key, associated with object
     * @return object according to the key
     */
    @Override
    public V get(Integer key) {
        Pair<V> pair = storage[key % storage.length];
        while(pair != null) {
            if (pair.key.equals(key)) {
                return pair.value;
            }
            pair = pair.nextPair;
        }
        return null;
    }

    /**
     * Replaces value by key with new object
     * @param key - key
     * @param value - new object
     * @return object, associated to the key before replacing
     */
    @Override
    public V update(Integer key, V value) {
        Pair<V> pair = storage[key % storage.length];
        while(pair != null) {
            if (pair.key.equals(key)) {
                V oldValue = pair.value;
                pair.value = value;
                return oldValue;
            }
            pair = pair.nextPair;
        }
        return null;
    }

    /**
     * Deletes key-value pair
     * @param key - key
     * @return object, associated to the key
     */
    @Override
    public V delete(Integer key) {
        int index = key % storage.length;
        if(storage[index] == null) {
            return null;
        }
        else {
            if(storage[index].key.equals(key)) {
                Pair<V> pair = storage[index];
                storage[index] = storage[index].nextPair;
                return pair.value;
            }
            else {
                Pair<V> pair = storage[index];
                while(pair.nextPair != null) {
                    Pair<V> previouslyPair = pair;
                    pair = pair.nextPair;
                    if(pair.key.equals(key)) {
                        previouslyPair.nextPair = pair.nextPair;
                        return pair.value;
                    }
                }
                return null;
            }
        }
    }

    private class Pair<T> {

        private final Integer key;
        private T value;
        private Pair<T> nextPair;

        /**
         * Creates new Pair object
         * @param key - key
         * @param value - value
         */
        private Pair(Integer key, T value) {
            this.key = key;
            this.value = value;
        }
    }
}
