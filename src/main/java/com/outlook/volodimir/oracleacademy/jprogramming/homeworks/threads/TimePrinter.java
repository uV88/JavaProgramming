package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.threads;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Scanner;

/**
 * Created by volodimir on 02.03.17.
 */
public class TimePrinter {

    public static final long SECOND = 1000L;
    public static final ZoneId UKRAINE_ZONE_ID = ZoneId.of("GMT+2");

    /**
     * Main method
     * @param args - string parameters
     */
    public static void main(String[] args) {
        startAndStopThread(new Thread("Time Printer 1") {
            @Override
            public void run() {
                printTimeEverySecond();
            }
        });
        startAndStopThread(new Thread(TimePrinter::printTimeEverySecond, "Time Printer 2"));
    }

    /**
     * Prints current time every second until thread is interrupted
     */
    private static void printTimeEverySecond() {
        long savedTime = 0L;
        while(!Thread.interrupted()) {
            long currentTime = System.currentTimeMillis();
            if((currentTime - savedTime) >= TimePrinter.SECOND) {
                System.out.println(Thread.currentThread().getName() + " -> " +
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(currentTime), TimePrinter.UKRAINE_ZONE_ID));
                savedTime = currentTime;
            }
        }
    }

    /**
     * Starts thread and then marks it as interrupted if any input from keyboard takes place
     * @param thread - thread
     */
    private static void startAndStopThread(Thread thread) {
        thread.start();
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextLine()) {
            thread.interrupt();
        }
    }
}
