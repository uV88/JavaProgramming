package com.outlook.volodimir.oracleacademy.jprogramming.lessons.collections;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Created by volodimir on 14.02.17.
 */
public class WordsOccurrence {

    public static final String SEPARATE_WORD_PATTERN = "\\b['\\w]+\\b";

    private final String englishText;
    private Map<String, Integer> wordsAppearance;

    /**
     * Creates WordsOccurrence object
     * @param fileName - full name of file
     * @param charsetName - name of charset of file
     */
    public WordsOccurrence(String fileName, String charsetName) throws IOException {
        try(BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charsetName))) {
            StringBuilder readText = new StringBuilder();
            String readString;
            while ((readString = bufReader.readLine()) != null) {
                readText.append(readString).append(System.getProperty("line.separator"));
            }
            englishText = readText.toString();
        }
        catch(UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }

    /**
     * Returns map where key is some word and value is number of this word appearance in the stated text
     * @return map where key is some word and value is number of this word appearance
     */
    public Map<String, Integer> countsWordsOccurrence() {
        if(wordsAppearance == null) {
            wordsAppearance = new HashMap<>();
            Matcher separateWordMatcher = Pattern.compile(WordsOccurrence.SEPARATE_WORD_PATTERN).matcher(englishText);
            while (separateWordMatcher.find()) {
                String word = separateWordMatcher.group();
                Integer occurrenceNumber;
                if ((occurrenceNumber = wordsAppearance.put(word, 1)) != null) {
                    wordsAppearance.put(word, occurrenceNumber + 1);
                }
            }
        }
        return wordsAppearance;
    }

    /**
     * Returns map where key is some word and value is number of this word appearance in the stated text.
     * Entries of map are sorted according to the established order.
     * @param order - established order
     * @return map where key is some word and value is number of this word appearance
     */
    public Map<String, Integer> countsWordsOccurrence(SortOrder order) {
        Comparator<Map.Entry<String, Integer>> comparator;
        switch (order) {
           case BY_KEY_FROM_A_TO_Z:
               comparator = Comparator.comparing(Map.Entry::getKey);
               break;
           case BY_KEY_FROM_Z_TO_A:
               comparator = Comparator.comparing(Map.Entry::getKey, Comparator.reverseOrder());
               break;
           case BY_VALUE_FROM_MIN_TO_MAX:
               comparator = Comparator.comparing(Map.Entry::getValue);
               break;
           case BY_VALUE_FROM_MAX_TO_MIN:
               comparator = Comparator.comparing(Map.Entry::getValue, Comparator.reverseOrder());
               break;
           default: throw new IllegalArgumentException();
        }
        Map<String, Integer> map = new LinkedHashMap<>();
        Stream<Map.Entry<String, Integer>> stream = countsWordsOccurrence().entrySet().stream();
        stream.sorted(comparator).forEach(e -> map.put(e.getKey(), e.getValue()));
        return map;
    }

    /**
     * SortOrder class represents different ways of sort of student list
     */
    enum SortOrder{
        BY_KEY_FROM_A_TO_Z, BY_KEY_FROM_Z_TO_A, BY_VALUE_FROM_MIN_TO_MAX, BY_VALUE_FROM_MAX_TO_MIN
    }
}
