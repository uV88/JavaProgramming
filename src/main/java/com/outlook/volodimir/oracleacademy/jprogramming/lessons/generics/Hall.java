package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

/**
 * Created by volodimir on 07.02.17.
 */
public class Hall extends Entity<Integer> {

    private final String hallName;
    private final int seatsNumber;
    private final int[] seatsScheme;

    /**
     * Creates new Hall object
     * @param id - Hall object identification
     * @param hallName - name of the hall
     * @param seatsNumber - number of seats in the hall
     * @param seatsScheme - seats disposition in the hall
     */
    public Hall(Integer id, String hallName, int seatsNumber, int[] seatsScheme) {
        super(id);
        this.hallName = hallName;
        this.seatsNumber = seatsNumber;
        this.seatsScheme = new int[seatsScheme.length];
        System.arraycopy(seatsScheme, 0, this.seatsScheme, 0, seatsScheme.length);
    }

    public String getHallName() {
        return hallName;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public int[] getSeatsSchemeCopy() {
        int[] exportScheme = new int[this.seatsScheme.length];
        System.arraycopy(this.seatsScheme, 0, exportScheme, 0, this.seatsScheme.length);
        return exportScheme;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "id=" + getId() +
                ", hallName='" + hallName + '\'' +
                ", seatsNumber=" + seatsNumber +
                '}';
    }
}
