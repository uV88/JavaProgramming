package com.outlook.volodimir.oracleacademy.jprogramming.lessons.threads;

import java.io.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodimir on 28.02.17.
 */
public class MultiThreadFileProcessor implements AutoCloseable {

    public static final String CHARSET_NAME = "utf8";
    public static final int COPY_BUFFER_SIZE = 1000;

    private final BufferedWriter bufferedLogWriter;

    /**
     * Creates new MultiThreadFileProcessor object having open buffered output character stream of the log file
     * @param logFile - file for logging
     */
    public MultiThreadFileProcessor(File logFile) {
        try {
            bufferedLogWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(logFile), MultiThreadFileProcessor.CHARSET_NAME));
        }
        catch (FileNotFoundException | UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }

    /**
     * Searches files in some directory and all of it's subdirectories with applying some pattern to the file name
     * @param pattern - pattern for applying to the file name
     * @param directory - directory, searching provides in
     */
    public void searchFile(Pattern pattern, File directory) {
        Thread newThread = lookDirectoryWithNewThread(directory, file -> {
            if (file.isDirectory()) {
                searchFile(pattern, file);
            } else {
                Matcher matcher = pattern.matcher(file.getName());
                if (matcher.matches()) {
                    try {
                        writeLog(file.getCanonicalPath());
                    } catch (IOException e) {
                        writeLog(e.getMessage());
                    }
                }
            }
        });
        try{
            newThread.join();
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void writeLog(String log) {
        try{
            bufferedLogWriter.write(log);
            bufferedLogWriter.newLine();
        }
        catch(IOException e) {
            throw new Error(e);
        }
    }

    /**
     * Copies all subdirectories and files of some directory to another directory
     * @param from - original directory
     * @param to - destination directory
     */
    public void copyDirectory(File from, File to) {
        lookDirectoryWithNewThread(from, file -> {
            File fileCopy = new File(to, file.getName());
            if (file.isDirectory()) {
                if(fileCopy.mkdir()) {
                    copyDirectory(file, fileCopy);
                }
                else {
                    throw new Error("Can't create directory " + fileCopy.getName());
                }
            } else {
                copyFile(file, fileCopy);
            }
        });
    }

    private void copyFile(File from, File to) {
        try(BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(from));
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(to))) {
            byte[] bytes = new byte[MultiThreadFileProcessor.COPY_BUFFER_SIZE];
            int readNumber;
            while((readNumber = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, readNumber);
            }
        }
        catch(IOException e) {
            throw new Error(e);
        }
    }

    private Thread lookDirectoryWithNewThread(File directory, Consumer<File> consumer) {
        Thread thread = new Thread(() -> {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    consumer.accept(file);
                }
            }
        });
        thread.start();
        return thread;
    }

    /**
     * Closes buffered output character stream of the log file
     */
    @Override
    public void close() {
        try {
            bufferedLogWriter.flush();
            bufferedLogWriter.close();
        }
        catch (IOException e) {
            throw new Error(e);
        }
    }

    /**
     * Main method
     * @param args - string parameters
     */
    public static void main(String[] args) {
        try(MultiThreadFileProcessor mtfProcessor = new MultiThreadFileProcessor(
                new File("./threadsLesson/log"))) {
            mtfProcessor.searchFile(Pattern.compile(".*text.*"), new File("./threadsLesson/directory"));
            mtfProcessor.copyDirectory(new File("./threadsLesson/directory"),
                    new File("./threadsLesson/directoryCopy"));
        }
    }
}
