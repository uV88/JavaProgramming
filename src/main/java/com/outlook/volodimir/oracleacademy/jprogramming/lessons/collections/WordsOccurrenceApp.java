package com.outlook.volodimir.oracleacademy.jprogramming.lessons.collections;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by volodimir on 16.02.17.
 */
public class WordsOccurrenceApp {

    public static void main(String[] args) throws IOException {
        WordsOccurrence wordsOccurrence = new WordsOccurrence("Romeo.txt", "utf8");
        printMap(wordsOccurrence.countsWordsOccurrence());
        printMap(wordsOccurrence.countsWordsOccurrence(WordsOccurrence.SortOrder.BY_KEY_FROM_A_TO_Z));
        printMap(wordsOccurrence.countsWordsOccurrence(WordsOccurrence.SortOrder.BY_KEY_FROM_Z_TO_A));
        printMap(wordsOccurrence.countsWordsOccurrence(WordsOccurrence.SortOrder.BY_VALUE_FROM_MIN_TO_MAX));
        printMap(wordsOccurrence.countsWordsOccurrence(WordsOccurrence.SortOrder.BY_VALUE_FROM_MAX_TO_MIN));
    }

    private static void printMap(Map<String, Integer> map) {
        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
        for(Map.Entry<String, Integer> entry : entrySet) {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }
        System.out.println("----------------------------------------------------");
    }
}
