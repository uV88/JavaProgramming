package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.stringprocessing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodimir on 29.01.17.
 */
public class MarkdownParser {

    /**
     * Returns HTML document created on the base of the Markdown document
     * @param markdownText - String object, represented Markdown document
     * @return String object, represented HTML document
     */
    public static String parseString(String markdownText) {
        StringBuilder stringBuilder = new StringBuilder(markdownText.length() + 100);
        stringBuilder.append("<html>\n<body>\n");

        Pattern headerPattern = Pattern.compile("(#+)([^#]+)"); //for #, ##, ...
        Pattern emPattern = Pattern.compile("[^\\*]\\*([^\\*]+?)\\*[^\\*]"); //for *...*
        Pattern strongPattern = Pattern.compile("[^\\*]\\*{2}([^\\*]+?)\\*{2}[^\\*]"); //for **...**
        Pattern linkPattern = Pattern.compile("\\[(.+?)\\]\\((.+?)\\)"); //for [...](...)

        for(String str : markdownText.split("\\R")) {
            Matcher headerMatcher = headerPattern.matcher(str);
            if(headerMatcher.matches()) { //finds #... (or ##..., ###..., ...)
                int level = headerMatcher.group(1).length(); //count of '#'
                stringBuilder.append(headerMatcher.replaceAll("<h" + level + ">$2</h" + level + ">"));
            }
            else{ //simple string
                stringBuilder.append("<p>");

                String parsedString = emPattern.matcher(str).replaceAll(" <em>$1</em> ");
                parsedString = strongPattern.matcher(parsedString).replaceAll(" <strong>$1</strong> ");
                parsedString = linkPattern.matcher(parsedString).replaceAll("<a href=\"$2\">$1</a>");

                stringBuilder.append(parsedString);
                stringBuilder.append("</p>");
            }
            stringBuilder.append("\n");
        }
        stringBuilder.append("</body>\n</html>\n");
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        String htmlString = MarkdownParser.parseString("##Header line\n" +
                "Simple line *with* em\n" +
                "Simple **line** with strong\n" +
                "Line with link [Link to google](https://www.google.com) in center\n" +
                "Line **with** *many* **elements** and link [Link to FB](https://www.facebook.com)");
        System.out.println(htmlString);
    }
}
