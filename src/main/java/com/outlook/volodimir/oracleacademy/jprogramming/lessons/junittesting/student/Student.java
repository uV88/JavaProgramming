package com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting.student;

/**
 * Created by volodimir on 25.01.17.
 */
public class Student {

    private String name;
    private String surname;
    private Group group;
    private PassedExamination[] passedExaminations;
    private int indexForNewExam = 0;

    public Student(String name, String surname, Group group, int numberOfExamin) {
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.passedExaminations = new PassedExamination[numberOfExamin];
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Group getGroup() {
        return group;
    }

    public boolean passExamination(PassedExamination examination) {
        boolean passed = false;
        if(indexForNewExam < passedExaminations.length) {
            passedExaminations[indexForNewExam] = examination;
            indexForNewExam++;
            passed = true;
        }
        return passed;
    }

    public void deleteFromPassed(String subject, int year, int semester) {
        boolean noExamination = true;
        for(int i = 0; i < indexForNewExam; i++) {
            if(passedExaminations[i].getSubject().equals(subject) && passedExaminations[i].getYear() == year && passedExaminations[i].getSemester() == semester) {
                passedExaminations[i] = null;
                System.arraycopy(passedExaminations, i + 1, passedExaminations, i, indexForNewExam - i - 1);
                passedExaminations[indexForNewExam - 1] = null;
                indexForNewExam--;
                noExamination = false;
                break;
            }
        }
        if(noExamination) throw new IllegalArgumentException("No examination in the list");
    }

    public int maxMarkIn(String subject) {
        int maxMarkForSubject = -1;
        for(int i = 0; i < indexForNewExam; i++) {
            if(passedExaminations[i].getSubject().equals(subject) && passedExaminations[i].getMark() > maxMarkForSubject) {
                maxMarkForSubject = passedExaminations[i].getMark();
            }
        }
        return maxMarkForSubject;
    }

    public int numOfExaminationsWith(int mark) {
        int count = 0;
        for(int i = 0; i < indexForNewExam; i++) {
            if(passedExaminations[i].getMark() == mark) {
                count++;
            }
        }
        return count;
    }

    public int meanMarkOf(int year, int semester) {
        int markSum = 0;
        int count = 0;
        for(int i = 0; i < indexForNewExam; i++) {
            if(passedExaminations[i].getYear() == year && passedExaminations[i].getSemester() == semester) {
                markSum += passedExaminations[i].getMark();
                count++;
            }
        }
        return markSum/count;
    }

}
