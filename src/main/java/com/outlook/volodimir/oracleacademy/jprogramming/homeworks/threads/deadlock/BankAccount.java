package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.threads.deadlock;

/**
 * Created by volodimir on 05.03.17.
 */
public class BankAccount {

    private long accountId;
    private double accountSum;

    /**
     * Creates new bank account
     * @param accountId - unique account identifier
     * @param accountSum - initial sum of money
     */
    public BankAccount(long accountId, double accountSum) {
        this.accountId = accountId;
        this.accountSum = accountSum;
    }

    public long getAccountId() {
        return accountId;
    }

    public double getAccountSum() {
        return accountSum;
    }

    /**
     * Remits money from this bank account to another
     * @param recipient - money recipient
     * @param sum - sum of remittance
     */
    public synchronized void remitMoney(BankAccount recipient, double sum) {
        if(sum < 0) throw new IllegalArgumentException();
        if((accountSum - sum) < 0) throw new IllegalArgumentException();
        System.out.println(Thread.currentThread().getName() + "-> Takes money...");
        accountSum -= sum;
        System.out.println(Thread.currentThread().getName() + "-> Sleeps...");
        try{
            Thread.sleep(1000);
        }catch(InterruptedException ex){
            System.err.println("Interrupted!");
        }
        System.out.println(Thread.currentThread().getName() + "-> Gives money...");
        synchronized(recipient) {
            recipient.accountSum += sum;
        }
        System.out.println(Thread.currentThread().getName() + "-> Finished!");
    }
}
