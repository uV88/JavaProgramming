package com.outlook.volodimir.oracleacademy.jprogramming.lessons.threads;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by volodimir on 28.02.17.
 */
public class MyShedule {

    /**
     * Prints each value (String type) from the map after number of milliseconds represented by key (Long type)
     * associated to value
     * @param map - map, contained pairs "time interval in milliseconds - message"
     */
    public static void printMessages(Map<Long, String> map) {
        map.forEach((timeInterval, message) -> {
            Thread messagePrinter = new Thread(() -> {
                try {
                    Thread.sleep(timeInterval);
                    System.out.println(message + " " + Thread.currentThread().getName());
                }
                catch(InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            });
            messagePrinter.start();
        });
    }

    /**
     * Main method
     * @param args - string parameters
     */
    public static void main(String[] args) {
        Map<Long, String> map = new HashMap<>();
        map.put(500L, "0.5s");
        map.put(1000L, "1.0s");
        map.put(1500L, "1.5s");
        map.put(3500L, "3.5s");
        map.put(5500L, "5.5s");
        map.put(9500L, "9.5s");
        map.put(12000L, "12.0s");
        printMessages(map);
    }
}
