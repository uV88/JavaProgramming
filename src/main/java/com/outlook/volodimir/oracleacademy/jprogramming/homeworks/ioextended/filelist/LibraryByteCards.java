package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.ioextended.filelist;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by volodimir on 23.02.17.
 */
public class LibraryByteCards implements FileListExplorer<Book> {

    private final File bookCardsFile;
    private final List<Book> bookCards = new ArrayList<>();

    /**
     * Creates new LibraryByteCards object associated to the book cards file
     * @param bookCardsFile - book cards file
     * @throws IOException - if an I/O error occurs
     */
    public LibraryByteCards(File bookCardsFile) throws IOException {
        this.bookCardsFile = bookCardsFile;
        refresh();
    }

    /**
     * Creates new LibraryByteCards object associated to the book cards file
     * @param fileName - name of the book cards file
     * @throws IOException - if an I/O error occurs
     */
    public LibraryByteCards(String fileName) throws IOException {
        this(new File(fileName));
    }

    /**
     * Refreshes content of list of this LibraryByteCards object according to actual state of book cards file
     * @throws IOException - if an I/O error occurs
     */
    @Override
    public final void refresh() throws IOException {
        bookCards.clear();
        try (ObjectInput input = new ObjectInputStream(
                new BufferedInputStream(new FileInputStream(bookCardsFile)))) {
            int count = input.readInt();
            for(int i = 0; i < count; i++) {
                bookCards.add((Book) input.readObject());
            }
        }
        catch(FileNotFoundException e) {
            /* keep working */
        }
        catch (ClassNotFoundException e) {
            /* Method refresh() can't throw ClassNotFoundException.
             * This is because of signature of method refresh() in FileListExplorer interface.
             * I imagine that I can't rewrite FileListExplorer interface.
             */
            throw new Error(e);
        }
    }

    /**
     * Adds new Book to the list of this LibraryByteCards object
     * and rewrites book cards file according to object list
     * @param bookCard - Book object
     * @throws IOException - if an I/O error occurs
     */
    @Override
    public void addItem(Book bookCard) throws IOException {
        bookCards.add(bookCard);
        try(ObjectOutput output = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(bookCardsFile)))) {
            output.writeInt(bookCards.size());
            for(Book book : bookCards) {
                output.writeObject(book);
            }
        }
    }

    /**
     * Returns list reflecting content of associated book cards file
     * at the moment of refresh() or addItem() methods last call
     * (or, if they weren't call directly, at the moment of this LibraryByteCards object creating)
     * @return list reflecting content of associated book cards file
     */
    @Override
    public List<Book> exportList() {
        return new ArrayList<>(bookCards);
    }
}
