package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

import java.util.Arrays;

/**
 * Created by volodimir on 09.02.17.
 */
public class Main {

    public static void main(String[] args) {
        //creates storage
        GenericStorage<Integer, Hall> hallStorage = new CombinedStorage<>();

        //creates hall's scheme of seats
        int[] scheme = new int[1005];
        Arrays.fill(scheme, 100);

        //prepares storage
        Hall emptyHall = new Hall(999, "", 0, scheme);
        for(int i = 0; i < 50; i++) {
            hallStorage.add(emptyHall);
        }

        //tests
        Integer[] keys = new Integer[5];
        keys[0] = hallStorage.add(new Hall(101, "Red", 100500, scheme)); // 50
        keys[1] = hallStorage.add(new Hall(102, "Blue", 100500, scheme)); // 51
        keys[2] = hallStorage.add(new Hall(103, "Green", 100500, scheme)); // 52
        keys[3] = hallStorage.add(new Hall(104, "Yellow", 100500, scheme)); // 53
        keys[4] = hallStorage.add(new Hall(105, "White", 100500, scheme)); // 54

        for(int key : keys) {
            System.out.println(key + " -> " + hallStorage.get(key));
        }
        System.out.println("----------------------------------------------------------");
        System.out.println("update by key " + keys[2] +
                ", old value: " + hallStorage.update(keys[2],
                new Hall(203, "Black", 100500, scheme)));
        System.out.println("----------------------------------------------------------");
        for(int key : keys) {
            System.out.println(key + " -> " + hallStorage.get(key));
        }
        System.out.println("----------------------------------------------------------");
        System.out.println("delete by key " + keys[0] + ", deleted value: " + hallStorage.delete(keys[0]));
        System.out.println("----------------------------------------------------------");
        for(int key : keys) {
            System.out.println(key + " -> " + hallStorage.get(key));
        }
    }
}
