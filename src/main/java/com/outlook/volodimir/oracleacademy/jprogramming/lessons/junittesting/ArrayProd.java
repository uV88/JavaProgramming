package com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting;

/**
 * Created by volodimir on 25.01.17.
 */
public class ArrayProd {
    private int[] instanceArray;

    public ArrayProd(int[] instanceArray) {
        this.instanceArray = instanceArray;
    }

    public static int product(int[] array) {
        int product = 1;
        for (int i = 0; i < array.length; i++) {
            product *= array[i];
        }
        return product;
    }

    public int product() {
        return ArrayProd.product(this.instanceArray);
    }
}
