package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.ioextended.filelist;

import java.io.IOException;

/**
 * Created by volodimir on 22.02.17.
 */
public class LibraryApp {

    public static void main(String[] args) throws IOException {
        FileListExplorer<Book> libraryCards = new LibraryCards("./ioHomework/bookList/libraryCards");
        testFileListExplorer(libraryCards);

        FileListExplorer<Book> libraryByteCards = new LibraryByteCards("./ioHomework/bookList/libraryByteCards");
        testFileListExplorer(libraryByteCards);
    }

    private static void testFileListExplorer(FileListExplorer<Book> fileListExplorer) throws IOException {
        System.out.println("-------------------------||---------------------------");
        fileListExplorer.exportList().forEach(System.out::println);
        System.out.println("------------------------------------------------------");
        fileListExplorer.addItem(new Book("The City and the Stars", "Arthur C. Clarke", 1956));
        fileListExplorer.addItem(new Book("A Fall of Moondust", "Arthur C. Clarke", 1961));
        fileListExplorer.addItem(new Book("2001: A Space Odyssey", "Arthur C. Clarke", 1968));
        fileListExplorer.addItem(new Book("The Fountains of Paradise", "Arthur C. Clarke", 1979));
        fileListExplorer.addItem(new Book("The Hammer of God", "Arthur C. Clarke", 1993));

        fileListExplorer.exportList().forEach(System.out::println);
        System.out.println("------------------------------------------------------");
        fileListExplorer.refresh();
        fileListExplorer.exportList().forEach(System.out::println);
        System.out.println("--------------------------||--------------------------");
    }
}
