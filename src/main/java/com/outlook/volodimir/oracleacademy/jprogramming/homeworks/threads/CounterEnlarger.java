package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.threads;

/**
 * Created by volodimir on 05.03.17.
 */
public class CounterEnlarger implements  Runnable{

    private static Counter counter = new Counter();

    @Override
    public void run() {
        System.out.println(CounterEnlarger.counter.first - CounterEnlarger.counter.second);
        CounterEnlarger.counter.first++;
        try{
            Thread.sleep(10);
        }
        catch(InterruptedException e) {
            System.err.println("Interrupted!");
        }
        CounterEnlarger.counter.second++;
        run();
        //Exception in thread "Thread-0" java.lang.StackOverflowError
        //Exception in thread "Thread-1" java.lang.StackOverflowError
        //Exception in thread "Thread-2" java.lang.StackOverflowError
    }

    public static void main(String[] args) {
        new Thread(new CounterEnlarger()).start();
        new Thread(new CounterEnlarger()).start();
        new Thread(new CounterEnlarger()).start();

    }

    private static class Counter{

        int first = 0;
        int second = 0;

    }
}
