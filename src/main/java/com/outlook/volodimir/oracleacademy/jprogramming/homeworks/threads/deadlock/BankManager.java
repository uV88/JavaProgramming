package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.threads.deadlock;

/**
 * Created by volodimir on 05.03.17.
 */
public class BankManager implements Runnable {

    public static final double MIN_TRANSFER_SUM = 1.0;

    private BankAccount sender;
    private BankAccount recipient;

    /**
     * Creates new BankManager object
     * @param sender - money sender
     * @param recipient - money recipient
     */
    public BankManager(BankAccount sender, BankAccount recipient ) {
        this.sender = sender;
        this.recipient  = recipient ;
    }

    @Override
    public void run() {
        sender.remitMoney(recipient , BankManager.MIN_TRANSFER_SUM);
    }

    /**
     * Main method
     * @param args - string parameters
     */
    public static void main(String[] args) {
        BankAccount account1 = new BankAccount(1L, 100500.0);
        BankAccount account2 = new BankAccount(2L, 100500.0);
        Thread hither = new Thread(new BankManager(account1, account2));
        Thread thither = new Thread(new BankManager(account2, account1));
        hither.start();
        thither.start();
    }
}
