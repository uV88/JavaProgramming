package com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting.student;

/**
 * Created by volodimir on 25.01.17.
 */
public class PassedExamination {

    private String subject;
    private int mark;
    private int year;
    private int semester;

    public PassedExamination(String subject, int mark, int year, int semester) {
        this.subject = subject;
        this.mark = mark;
        this.year = year;
        this.semester = semester;
    }

    public String getSubject() {
        return subject;
    }

    public int getMark() {
        return mark;
    }

    public int getYear() {
        return year;
    }

    public int getSemester() {
        return semester;
    }
}
