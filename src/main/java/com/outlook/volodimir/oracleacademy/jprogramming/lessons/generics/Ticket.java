package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

/**
 * Created by volodimir on 07.02.17.
 */
public class Ticket extends Entity<Integer> {

    private final Performance performance;
    private final Visitor visitor;
    private final int rowNumber;
    private final int seatNumber;
    private final int price;

    /**
     * Creates new Ticket object
     * @param id - Ticket object identification
     * @param performance - performance, this ticket for
     * @param visitor - ticket's owner
     * @param rowNumber - number of the row in the hall
     * @param seatNumber - number of the seat in the row
     * @param price - ticket's price
     */
    public Ticket(Integer id, Performance performance, Visitor visitor, int rowNumber, int seatNumber, int price) {
        super(id);
        this.performance = performance;
        this.visitor = visitor;
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.price = price;
    }

    public Performance getPerformance() {
        return performance;
    }

    public Visitor getVisitor() {
        return visitor;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + getId() +
                ", performance=" + performance +
                ", visitor=" + visitor.getLogin() +
                ", rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", price=" + price +
                '}';
    }
}
