package com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting;

/**
 * Created by volodimir on 24.01.17.
 */
public class ArraySum {
    private int[] instanceArray;

    public ArraySum(int[] instanceArray) {
        this.instanceArray = instanceArray;
    }

    public static int sum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    public int sum() {
        return ArraySum.sum(this.instanceArray);
    }
}
