package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.io;

import java.io.File;
import java.io.IOException;

/**
 * Created by volodimir on 22.02.17.
 */
public interface Translator {

    /**
     * Prints translated content of text file
     * @param file - text file
     * @throws IOException - if text file doesn't exist or if an I/O error occurs
     */
    void translate(File file) throws IOException;

}
