package com.outlook.volodimir.oracleacademy.jprogramming.lessons.stringparsing;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodimir on 31.01.17.
 */
public class HTMLParser {

    public static String RATING_POSITION_GROUP = "position";
    public static String RATING_MALE_NAME_GROUP = "maleName";
    public static String RATING_FEMALE_NAME_GROUP = "femaleName";
    public static String LAPTOP_ID_GROUP = "id";
    public static String LAPTOP_NAME_GROUP = "name";
    public static String LAPTOP_DESCRIPTION_GROUP = "description";
    public static String LAPTOP_PRICE_GROUP = "price";

    private String htmlString;

    public HTMLParser(String fileName, String charsetName) throws IOException {
        try(BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charsetName))) {
            String readString;
            StringBuilder readText = new StringBuilder();
            while ((readString = bufReader.readLine()) != null) {
                readText.append(readString);
            }
            this.htmlString = readText.toString();
        }
        catch(UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }

    public List<Rating> parseHTMLForRatings(String regExp) {
        Matcher ratingMatcher = Pattern.compile(regExp).matcher(this.htmlString);
        List<Rating> ratings = new ArrayList<>();
        while(ratingMatcher.find()) {
            int position = Integer.parseInt(ratingMatcher.group(HTMLParser.RATING_POSITION_GROUP));
            String maleName = ratingMatcher.group(HTMLParser.RATING_MALE_NAME_GROUP);
            String femaleName = ratingMatcher.group(HTMLParser.RATING_FEMALE_NAME_GROUP);
            ratings.add(new Rating(position, maleName, femaleName));
        }
        return ratings;
    }

    public List<Laptop>  parseHTMLForLaptops(String regExp) {
        Matcher laptopMatcher = Pattern.compile(regExp).matcher(this.htmlString);
        List<Laptop> laptops = new ArrayList<>();
        while(laptopMatcher.find()) {
            int id = Integer.parseInt(laptopMatcher.group(HTMLParser.LAPTOP_ID_GROUP));
            String name = laptopMatcher.group(HTMLParser.LAPTOP_NAME_GROUP);
            String description = laptopMatcher.group(HTMLParser.LAPTOP_DESCRIPTION_GROUP);
            int price = Integer.parseInt(laptopMatcher.group(HTMLParser.LAPTOP_PRICE_GROUP));
            laptops.add(new Laptop(id, name, description, price));
        }
        return laptops;
    }

    static class Rating {

        private int position;
        private String maleName;
        private String femaleName;

        public Rating(int position, String maleName, String femaleName) {
            this.position = position;
            this.maleName = maleName;
            this.femaleName = femaleName;
        }

        public int getPosition() {
            return position;
        }

        public String getMaleName() {
            return maleName;
        }

        public String getFemaleName() {
            return femaleName;
        }

        @Override
        public String toString() {
            return "Rating{" +
                    "position=" + position +
                    ", maleName='" + maleName + '\'' +
                    ", femaleName='" + femaleName + '\'' +
                    '}';
        }
    }

    static class Laptop {

        private int id;
        private String name;
        private String description;
        private int price;

        public Laptop(int id, String name, String description, int price) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.price = price;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public int getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return "Laptop{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", description='" + description + '\'' +
                    ", price=" + price +
                    '}';
        }
    }

    public static void main(String[] args) throws IOException {
        HTMLParser parser1 = new HTMLParser("baby2008.html", "WINDOWS-1251");
        List<Rating> ratings = parser1.parseHTMLForRatings(
                "<td>(?<position>\\d+)<\\/td>" +
                        "<td>(?<maleName>\\w+)<\\/td>" +
                        "<td>(?<femaleName>\\w+)<\\/td>");
        for(Rating rating : ratings) {
            System.out.println(rating.toString());
        }

        HTMLParser parser2 = new HTMLParser("source.html", "WINDOWS-1251");
        List<Laptop> laptops = parser2.parseHTMLForLaptops(
                "<h6 class=\"name\"><a href.+?>(?<name>.+?)<\\/a><\\/h6>" +
                        "<p class=\"description\">(?<description>.+?)<br \\/>.+?<\\/p>.+?" +
                        "<span class=\"catalog-tovar-id\">.+?(?<id>\\d+)<\\/span>.+?" +
                        "<span class=\"price cost\">(?<price>\\d+).+?<\\/span>");
        for(Laptop laptop : laptops) {
            System.out.println(laptop.toString());
        }
    }
}
