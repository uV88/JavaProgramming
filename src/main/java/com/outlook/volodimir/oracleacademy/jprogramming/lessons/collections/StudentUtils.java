package com.outlook.volodimir.oracleacademy.jprogramming.lessons.collections;

import java.util.*;

/**
 * Created by volodimir on 14.02.17.
 */
public class StudentUtils {

    /**
     * Returns map where value is Student object and key is "first name last name" (values of Student object fields)
     * @param students - list of Student objects
     * @return map where value is Student object and key is "first name last name"
     */
    public static Map<String, Student> createMapFromList(List<Student> students) {
        Map<String, Student> studentMap = new HashMap<>();
        for(Student student : students) {
            studentMap.put(student.getFirstName() + " " + student.getLastName(),  student);
        }
        return studentMap;
    }

    /**
     * Prints names of students of stated course
     * @param students - list of students
     * @param course  - stated course
     */
    public static void printStudents(List<Student> students, int course) {
        Iterator<Student> iterator = students.iterator();
        while(iterator.hasNext()) {
            Student student = iterator.next();
            if(student.getCourse() == course) {
                System.out.println(student.getFirstName());
            }
        }
    }

    /**
     * Returns sorted by first name list of students
     * @param students - unsorted list of students
     * @return sorted by first name list of students
     */
    public static List<Student> sortStudent(List<Student> students) {
        List<Student> sortedStudents = new ArrayList<>(students);
        sortedStudents.sort(Comparator.comparing(Student::getFirstName));
        return sortedStudents;
    }
}
