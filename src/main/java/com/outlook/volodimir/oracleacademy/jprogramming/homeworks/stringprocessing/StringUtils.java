package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.stringprocessing;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodimir on 29.01.17.
 */
public class StringUtils {

    /**
     * Returns string having reverse order of characters
     * @param str - initial string
     * @return string having the same characters as 'str' which are disposed with reverse order
     */
    public static String turnOverString(String str) {
        return new StringBuilder(str).reverse().toString();
    }

    /**
     * Returns 'true' if string is palindrome
     * @param str - some string
     * @return 'true', if 'str' is palindrome, 'false' otherwise
     */
    public static boolean isPalindrome(String str) {
        StringBuilder strBuilder = new StringBuilder(str.length());
        Matcher match = Pattern.compile("\\w+").matcher(str.toLowerCase()); //only words, only lower case
        while(match.find()) {
            strBuilder.append(match.group());
        }
        String originalOrderString = strBuilder.toString();
        String reversedOrderString = strBuilder.reverse().toString();
        return originalOrderString.equals(reversedOrderString);
    }

    /**
     * Returns cut or increased string
     * @param str - initial string
     * @return substring of 'str' having 6 characters  -  if length of 'str' is more than 10 characters,
     * string of 12 characters: 'str' and some number of 'o' characters  -  if length of 'str' is 10 characters or less
     */
    public static String trimOrExtendString(String str) {
        String modifiedString;
        if(str.length() > 10) {
            modifiedString = str.substring(0, 6);
        }
        else {
            char[] additionalChars = new char[12 - str.length()];
            Arrays.fill(additionalChars, 'o');
            modifiedString = str.concat(new String(additionalChars));
        }
        return modifiedString;
    }

    /**
     * Returns string having exchanged first and last words
     * @param str - initial string
     * @return string with exchanged first and last words in comparison to the initial string
     */
    public static String exchangeFirstAndLastWords(String str) {
        Matcher matcher = Pattern.compile("(\\A.*?)(\\b.+?\\b)(.*)(\\b.+\\b)(.*\\z)").matcher(str);
        return matcher.replaceAll("$1$4$3$2$5");
    }

    /**
     * Returns string having exchanged first and last words in every sentence
     * @param str - initial string
     * @return string with exchanged first and last words in every sentence (ends with '.')
     * in comparison to the initial string
     */
    public static String exchangeFirstAndLastWordsOfSentence(String str) {
        StringBuilder stringBuilder = new StringBuilder(str.length() + 10);
        for(String sent : str.split("\\.[ ]*")) {
            stringBuilder.append(StringUtils.exchangeFirstAndLastWords(sent));
            stringBuilder.append(". ");
        }
        return stringBuilder.toString();
    }

    /**
     * Returns 'true' if string consists of 'a', 'b', 'c' only
     * @param str - some string
     * @return 'true' if string consists of 'a', 'b', 'c' only, 'false' otherwise
     */
    public static boolean isOnlyabc(String str) {
        return str.matches("[abc]+");
    }

    /**
     * Returns 'true' if string represents date with format MM.DD.YY
     * @param str - some string
     * @return 'true' if string represents date with format MM.DD.YY, 'false' otherwise
     */
    public static boolean isDateMMpDDpYYYY(String str) {
        boolean isDate = false;
        Matcher matcher = Pattern.compile("(\\d{2})\\.(\\d{2})\\.\\d{4}").matcher(str);
        if(matcher.matches()){
            int month = Integer.parseInt(matcher.group(1));
            int day = Integer.parseInt(matcher.group(2));
            if(month > 0 && month < 13 && day > 0 && day < 32) {
                isDate = true;
            }
        }
        return isDate;
    }

    /**
     * Returns 'true' if string represents email address
     * @param str - some string
     * @return 'true' if string represents email address, 'false' otherwise
     */
    public static boolean isEmail(String str) {
        return str.matches("[a-zA-Z\\d\\.]+@[a-z]+\\.{1}[a-z]{1,3}");
    }

    /**
     * Returns array of strings - telephone numbers with format +Х(ХХХ)ХХХ-ХХ-ХХ
     * @param str - some string
     * @return String[] contained telephone numbers with format +Х(ХХХ)ХХХ-ХХ-ХХ
     */
    public static String[] extractTelephoneNumbers(String str) {
        Matcher match = Pattern.compile("\\+\\d\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}").matcher(str);
        int count = 0;
        while(match.find()) count++;
        match.reset();
        String[] telephoneNum = new String[count];
        for(int i = 0; i < telephoneNum.length; i++) {
            if(match.find()) {
                telephoneNum[i] = match.group();
            }
        }
        return telephoneNum;
    }
}
