package com.outlook.volodimir.oracleacademy.jprogramming.lessons.io.serialization;

import java.io.*;

/**
 * Created by volodimir on 26.02.17.
 */
public class SerializationManager {

    public static final String FILE_FOR_SERIALIZATION_NAME = "./ioLesson/serialization";

    public static void main(String[] args) {
        Group group = new Group("Electrotechnology", "Faraday");
        group.getStudents().add(new Student("Andre", "Ampere", 2));
        group.getStudents().add(new Student("Georg", "Ohm", 2));
        System.out.println(group);
        try(ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(SerializationManager.FILE_FOR_SERIALIZATION_NAME))){
            oos.writeObject(group);
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        Group dsGroup = null;
        try(ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(SerializationManager.FILE_FOR_SERIALIZATION_NAME))){
            dsGroup = (Group) ois.readObject();
        }
        catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(dsGroup);
        System.out.println(group == dsGroup);
        System.out.println(group.getStudents().get(0) == dsGroup.getStudents().get(0));
        System.out.println(group.getStudents().get(1) == dsGroup.getStudents().get(1));
    }
}
