package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by volodimir on 20.02.17.
 */
public class TranslatorApp {

    public static final String VOCABULARY_DIRECTORY = "./ioHomework/translator/vocabulary";
    public static final String TEXT_TO_TRANSLATE_DIRECTORY = "./ioHomework/translator/textFiles";
    public static final String CONFIRMED_WORD = "yes";

    /**
     * Organizes user-machine interaction to provide process of text files translation
     * @throws IOException - if vocabulary file or text file doesn't exist or if an I/O error occurs
     */
    public void translateWithVocabulary() throws IOException {
        File vocabularyFile = selectFileIn(TranslatorApp.VOCABULARY_DIRECTORY, "Select vocabulary by number");
        Translator translator = new TranslatorImpl(vocabularyFile);

        boolean translateAnother = true;
        while(translateAnother) {
            File textFile = selectFileIn(TranslatorApp.TEXT_TO_TRANSLATE_DIRECTORY, "Select text file by number");
            translator.translate(textFile);
            translateAnother = confirm("Have you any other text to translate with this vocabulary?");
        }

        if(confirm("Do you want to use another vocabulary?")) {
            translateWithVocabulary();
        }
    }

    private File selectFileIn(String directory, String motion) throws FileNotFoundException {
        File[] files = new File(directory).listFiles();
        if(files == null || files.length == 0) throw new FileNotFoundException("No files in " + directory);
        System.out.println("----------------------------------------------");
        for(int i = 0; i < files.length; i++) {
            System.out.println((i + 1) + " -> " + files[i].getName());
        }
        System.out.println("----------------------------------------------");
        System.out.println(motion);
        int selectedNumber = 1;
        Scanner scanner = new Scanner(System.in);
        if(scanner.hasNext()) {
            selectedNumber = scanner.nextInt();
        }
        return files[selectedNumber - 1];
    }

    private boolean confirm(String inquiry) {
        System.out.println(inquiry);
        String answer = "";
        Scanner scanner = new Scanner(System.in);
        if(scanner.hasNext()) {
            answer = scanner.next();
        }
        return answer.equals(TranslatorApp.CONFIRMED_WORD);
    }

    /**
     * Main method
     * @param args - String parameters
     * @throws IOException - if vocabulary file or text file doesn't exist or if an I/O error occurs
     */
    public static void main(String[] args) throws IOException {
        TranslatorApp translatorApp = new TranslatorApp();
        translatorApp.translateWithVocabulary();
    }
}
