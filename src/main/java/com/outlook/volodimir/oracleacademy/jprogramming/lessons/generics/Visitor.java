package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

/**
 * Created by volodimir on 07.02.17.
 */
public class Visitor extends Entity<Integer> {

    private final String login;
    private final String firstName;
    private final String lastName;
    private final String emailAddress;
    private final String password;
    private final Role role;

    /**
     * Creates new Visitor object
     * @param id - Visitor object identification
     * @param login - visitor's login
     * @param firstName - visitor's first name
     * @param lastName - visitor's last name
     * @param emailAddress - visitor's email address
     * @param password - visitor's password
     * @param role - visitor's role
     */
    public Visitor(Integer id, String login, String firstName, String lastName, String emailAddress, String password, Role role) {
        super(id);
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "Visitor{" +
                "id=" + getId() +
                ", login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public enum Role{USER, ADMIN}
}
