package com.outlook.volodimir.oracleacademy.jprogramming.lessons.threads;

/**
 * Created by volodimir on 28.02.17.
 */
public class ThreadsLoadingTest {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                for (int j = 0;; j++) {
                    double z = Math.cos(j);
                    double x = Math.cos(z);
                }
            }).start();
        }
    }
    /*public static void main(String[] args) {
        for (int i = 0; i < 100000; i++) {
            new Thread(() -> {
                for (int j = 0; ; j++) {
                    double z = 1 + j;
                }
            }).start();
        }
    }*/
}
