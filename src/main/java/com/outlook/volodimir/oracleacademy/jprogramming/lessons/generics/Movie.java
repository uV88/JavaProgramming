package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

/**
 * Created by volodimir on 07.02.17.
 */
public class Movie extends Entity<Integer> {

    private String movieTitle;
    private String originalMovieTitle;
    private String posterUrl;
    private String movieDescription;
    private String director;
    private String screenwriter;
    private String actors;

    /**
     * Creates new Movie object
     * @param id - Movie object identification
     * @param movieTitle - movie's title
     */
    public Movie(Integer id, String movieTitle) {
        super(id);
        this.movieTitle = movieTitle;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getOriginalMovieTitle() {
        return originalMovieTitle;
    }

    public void setOriginalMovieTitle(String originalMovieTitle) {
        this.originalMovieTitle = originalMovieTitle;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getScreenwriter() {
        return screenwriter;
    }

    public void setScreenwriter(String screenwriter) {
        this.screenwriter = screenwriter;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + getId() +
                ", movieTitle='" + movieTitle + '\'' +
                ", originalMovieTitle='" + originalMovieTitle + '\'' +
                ", director='" + director + '\'' +
                ", screenwriter='" + screenwriter + '\'' +
                ", actors='" + actors + '\'' +
                '}';
    }
}

