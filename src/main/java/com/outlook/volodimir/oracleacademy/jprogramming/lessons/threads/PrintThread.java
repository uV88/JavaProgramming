package com.outlook.volodimir.oracleacademy.jprogramming.lessons.threads;

/**
 * Created by volodimir on 28.02.17.
 */
public class PrintThread {

    public static void main(String[] args) {
        Thread printsNameThread = new Thread("threadPrinter") {
            @Override
            public void run() {
                PrintThread.print();
            }
        };
        printsNameThread.start();
        Thread printNameRunnable = new Thread(PrintThread::print, "runnablePrinter");
        printNameRunnable.start();
    }

    static private void print() {
        try {
            for (int i = 0; i < 10; i++) {
                Thread.sleep(500L);
                System.out.println(Thread.currentThread().getName());
            }
        }
        catch(InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
