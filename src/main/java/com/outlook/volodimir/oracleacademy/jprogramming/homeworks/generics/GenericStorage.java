package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.generics;

/**
 * Created by volodimir on 05.02.17.
 */
public class GenericStorage<E> {

    private Object[] storage;
    private int indexForNewElement = 0;

    /**
     * Creates new GenericStorage object having default capacity
     */
    public GenericStorage() {
        storage = new Object[10];
    }

    /**
     * Creates new GenericStorage object having specified capacity
     * @param size - GenericStorage object capacity
     */
    public GenericStorage(int size) {
        if(size < 0) throw new IllegalArgumentException("Storage size cannot be less than 0");
        storage = new Object[size];
    }

    /**
     * Adds new element to this GenericStorage object
     * @param obj - object to be added
     */
    public void add(E obj) {
        storage[indexForNewElement] = obj;
        if(++indexForNewElement == storage.length) {
            Object[] newStorage = new Object[storage.length * 2];
            System.arraycopy(storage, 0, newStorage, 0, storage.length);
            storage = newStorage;
        }
    }

    /**
     * Returns element, specified by index, from this GenericStorage object
     * @param index - index of element
     * @return object specified by index
     */
    public E get(int index) {
        if(index < 0 || index >= indexForNewElement) throw new IndexOutOfBoundsException("Index value isn't valid");
        return (E)storage[index];
    }

    /**
     * Returns all elements from this GenericStorage object
     * @return array of objects which are contained in this GenericStorage object
     */
    public E[] getAll() {
        Object[] exportedStorage = new Object[indexForNewElement];
        System.arraycopy(storage, 0, exportedStorage, 0, indexForNewElement);
        return (E[])exportedStorage;
    }

    /**
     * Updates element at specified index with specified object
     * @param index - index of element to update
     * @param obj - object to be stored at the specified index
     */
    public void update(int index, E obj) {
        if(index < 0 || index >= indexForNewElement) throw new IndexOutOfBoundsException("Index value isn't valid");
        storage[index] = obj;
    }

    /**
     * Deletes element at specified index
     * @param index - index of element to delete
     */
    public void delete(int index) {
        if(index < 0 || index >= indexForNewElement) throw new IndexOutOfBoundsException("Index value isn't valid");
        deleteElement(index);
    }

    /**
     * Deletes all elements which are corresponds to specified object
     * @param obj - specified object
     * @return 'true'  -  if at least one entry of specified object to this GenericStorage found and deleted
     *         'false'  -  if no entry of specified object to this GenericStorage found
     */
    public boolean delete(E obj) {
        boolean deleted = false;
        for(int i = 0; i < indexForNewElement; i++) {
            if(storage[i] == obj) {
                deleteElement(i);
                deleted = true;
            }
        }
        return deleted;
    }

    private void deleteElement(int index) {
        System.arraycopy(storage, (index + 1), storage, index, (indexForNewElement - 1 - index));
        storage[--indexForNewElement] = null;
    }

    /**
     * Returns number of elements of this GenericStorage object
     * @return number of elements
     */
    public int size() {
        return indexForNewElement;
    }
}
