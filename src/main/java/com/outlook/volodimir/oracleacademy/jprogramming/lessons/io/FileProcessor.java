package com.outlook.volodimir.oracleacademy.jprogramming.lessons.io;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodimir on 21.02.17.
 */
public class FileProcessor {

    public static final String CHARSET_NAME = "utf8";
    public static final Pattern STUDENT_MARK_PATTERN = Pattern.compile("(?<name>\\D+)=(?<mark>\\d+)");
    public static final Pattern FIRST_AND_LAST_IN_LINE = Pattern.compile(
            "(\\A.*?)(\\b.+?\\b)(.*)(\\b.+\\b)(.*\\z)");
    public static final Pattern FIRST_AND_LAST_IN_SENTENCE = Pattern.compile(
            "([^.?!]*?)(\\b[^.?!]+?\\b)([^.?!]*)(\\b[^.?!]+\\b)([^.?!]*[.?!])");
    public static final int COPY_BUFFER_SIZE = 1000;

    /**
     * Writes list of random integers to the text file
     * @param fileName - name of the file
     * @param count - size of list of random integers
     * @throws IOException - if an I/O error occurs
     */
    public void fillTextFileWithRandom(String fileName, int count) throws IOException {
        Random random = new Random();
        writeLinesToFile(fileName, count, i -> Integer.toString(random.nextInt()));
    }

    /**
     * Reads list of integers from text file,
     * sorts the list and rewrites it to the same file
     * @param fileName - name of the file
     * @throws IOException - if text file doesn't exist or an I/O error occurs
     */
    public void sortNumbersOfTextFile(String fileName) throws IOException {
        List<Integer> list = new ArrayList<>();
        readLinesFromFile(fileName, string -> list.add(Integer.parseInt(string)));
        Collections.sort(list);
        writeListToFile(fileName, list);
    }

    /**
     * Reads list of students marks from the text file, computes mean mark of each student,
     * prints student's name and mean mark if mean mark is equal to or exceeds minimal mark
     * @param minMark - minimal mark
     * @param fileName - name of the file
     * @throws IOException - if text file doesn't exist or an I/O error occurs
     */
    public void printMeanMarksAbove(int minMark, String fileName) throws IOException {
        Map<String, List<Integer>> studentsMarks = new HashMap<>();
        readLinesFromFile(fileName, string -> {
            Matcher studentMatcher = FileProcessor.STUDENT_MARK_PATTERN.matcher(string);
            if(studentMatcher.matches()) {
                String name = studentMatcher.group("name");
                Integer mark = Integer.parseInt(studentMatcher.group("mark"));
                List<Integer> marks = studentsMarks.computeIfAbsent(name, t -> new ArrayList<>());
                marks.add(mark);
            }
        });
        studentsMarks.forEach((name, marks) -> {
            int marksSum = 0;
            for(Integer mark : marks) {
                marksSum += mark;
            }
            int meanMark = marksSum / marks.size();
            if(meanMark >= minMark) {
                System.out.println(name + "=" + meanMark);
            }
        });
    }

    /**
     * Reads text from file, replaces first and last words in every line,
     * writes edited text to another file
     * @param fileName - name of file to read
     * @param destFileName - name of file to write
     * @throws IOException - if file to read doesn't exist or an I/O error occurs
     */
    public void replaceFirstAndLastInLine(String fileName, String destFileName) throws IOException {
        List<String> lines = new ArrayList<>();
        readLinesFromFile(fileName, line -> {
            Matcher matcher = FileProcessor.FIRST_AND_LAST_IN_LINE.matcher(line);
            lines.add(matcher.replaceAll("$1$4$3$2$5"));
        });
        writeListToFile(destFileName, lines);
    }

    /**
     * Reads text from file, replaces first and last words in every sentence,
     * writes edited text to another file
     * @param fileName - name of file to read
     * @param destFileName - name of file to write
     * @throws IOException - if file to read doesn't exist or an I/O error occurs
     */
    public void replaceFirstAndLastInSentence(String fileName, String destFileName) throws IOException {
        StringBuilder text = new StringBuilder();
        readLinesFromFile(fileName, line -> text.append(line).append(System.getProperty("line.separator")));
        Matcher matcher = FileProcessor.FIRST_AND_LAST_IN_SENTENCE.matcher(text);
        writeTextToFile(destFileName, matcher.replaceAll("$1$4$3$2$5"));
    }

    private void readLinesFromFile(String fileName, Consumer<String> consumer) throws IOException {
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                new FileInputStream(fileName), FileProcessor.CHARSET_NAME))) {
            String readString;
            while ((readString = bufferedReader.readLine()) != null) {
                consumer.accept(readString);
            }
        }
    }

    private void writeLinesToFile(String fileName, int count, Function<Integer, String> function) throws IOException {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(fileName), FileProcessor.CHARSET_NAME))){
            for(int i = 0; i < count; i++) {
                bufferedWriter.write(function.apply(i));
                bufferedWriter.newLine();
            }
        }
    }

    private<E> void writeListToFile(String fileName, List<E> list) throws IOException {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(fileName), FileProcessor.CHARSET_NAME))){
            for(E item : list) {
                bufferedWriter.write(item.toString());
                bufferedWriter.newLine();
            }
        }
    }

    private void writeTextToFile(String fileName, String text) throws IOException {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(fileName), FileProcessor.CHARSET_NAME))){
            bufferedWriter.write(text);
        }
    }

    /**
     * Copies file using buffered and non-buffered streams, compares time intervals
     * @param fileName - name of the original file
     * @param destFileName - name of the copy of the file
     * @throws IOException - if file doesn't exist or an I/O error occurs
     */
    public void compareCopyTime(String fileName, String destFileName) throws IOException {
        File file = new File(fileName);
        File destFile = new File(destFileName);

        //This is NOT RIGHT!
        long initialTime = System.nanoTime();
        copyWithBuffer(file, destFile);
        long endTime = System.nanoTime();
        System.out.println("Buffered: " + (endTime - initialTime));

        //This is NOT RIGHT!
        initialTime = System.nanoTime();
        copyWithoutBuffer(file, destFile);
        endTime = System.nanoTime();
        System.out.println("Non-buffered: " + (endTime - initialTime));
    }

    private void copyWithBuffer(File from, File to) throws IOException {
        try(BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(from));
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(to))) {
            copyBytes(inputStream, outputStream);
        }
    }

    private void copyWithoutBuffer(File from, File to) throws IOException {
        try(InputStream inputStream = new FileInputStream(from);
            OutputStream outputStream = new FileOutputStream(to)) {
            copyBytes(inputStream, outputStream);
        }
    }

    private void copyBytes(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bytes = new byte[FileProcessor.COPY_BUFFER_SIZE];
        int readNumber;
        while ((readNumber = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, readNumber);
        }

    }

}
