package com.outlook.volodimir.oracleacademy.jprogramming.lessons.io.serialization;

import java.io.Serializable;

/**
 * Created by volodimir on 26.02.17.
 */
public class Student implements Serializable {

    private String firstName;
    private String lastName;
    private int course;

    /**
     * Creates new Student object
     * @param firstName - first name of student
     * @param lastName - last name of student
     * @param course - student's course
     */
    public Student(String firstName, String lastName, int course) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.course = course;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getCourse() {
        return course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", course=" + course +
                '}';
    }
}
