package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by volodimir on 07.02.17.
 */
public class Performance extends Entity<Integer> {

    private final LocalDate date;
    private final LocalTime time;
    private final Hall hall;
    private final Movie movie;

    /**
     * Creates new Performance object
     * @param id - Performance object identification
     * @param date - date of the performance
     * @param time - time of the performance
     * @param hall - performance's hall
     * @param movie - performance's movie
     */
    public Performance(Integer id, LocalDate date, LocalTime time, Hall hall, Movie movie) {
        super(id);
        this.date = date;
        this.time = time;
        this.hall = hall;
        this.movie = movie;
    }

    public LocalDate getDate() {
        return date;
    }

    public LocalTime getTime() {
        return time;
    }

    public Hall getHall() {
        return hall;
    }

    public Movie getMovie() {
        return movie;
    }

    @Override
    public String toString() {
        return "Performance{" +
                "id=" + getId() +
                ", date=" + date +
                ", time=" + time +
                ", hall=" + hall.getHallName() +
                ", movie=" + movie.getMovieTitle() +
                '}';
    }
}
