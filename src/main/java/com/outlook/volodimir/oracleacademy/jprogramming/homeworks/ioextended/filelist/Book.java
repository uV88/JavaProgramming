package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.ioextended.filelist;

import java.io.Serializable;

/**
 * Created by volodimir on 22.02.17.
 */
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;
    private final String title;
    private final String author;
    private final int year;

    /**
     * Creates new Book object
     * @param title - book's title
     * @param author - author of the book
     * @param year - year of book publishing
     */
    public Book(String title, String author, int year) {
        this.title = title;
        this.author = author;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", year=" + year +
                '}';
    }
}
