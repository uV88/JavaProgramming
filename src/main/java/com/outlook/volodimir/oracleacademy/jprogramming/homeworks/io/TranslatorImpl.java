package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.io;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodimir on 19.02.17.
 */
public class TranslatorImpl implements Translator {

    public static final String CHARSET_NAME = "utf8";
    public static final String VOCABULARY_STRING_PATTERN = "(?<word>\\S+?);-;(?<translatedWord>\\S+)";
    public static final String SEPARATE_WORD_PATTERN = "\\b(?<word>\\S+?)\\b";

    private Map<String, String> vocabulary = new HashMap<>();

    /**
     * Creates new TranslatorImpl object associated to vocabulary file
     * @param file - vocabulary file
     * @throws IOException - if file of vocabulary doesn't exist or if an I/O error occurs
     */
    public TranslatorImpl(File file) throws IOException {
        Pattern pattern = Pattern.compile(TranslatorImpl.VOCABULARY_STRING_PATTERN);
        readLines(file, string -> {
            Matcher matcher = pattern.matcher(string);
            if(matcher.matches()) {
                vocabulary.put(matcher.group("word"), matcher.group("translatedWord"));
            }
        });
    }

    /**
     * Prints content of text file translated according to the vocabulary of this TranslatorImpl
     * @param file - text file
     * @throws IOException - if text file doesn't exist or if an I/O error occurs
     */
    @Override
    public void translate(File file) throws IOException {
        StringBuilder readText = new StringBuilder();
        readLines(file, string -> readText.append(string).append(System.getProperty("line.separator")));
        StringBuffer translatedText = new StringBuffer(readText.length());
        Matcher matcher = Pattern.compile(TranslatorImpl.SEPARATE_WORD_PATTERN).matcher(readText);
        while (matcher.find()) {
            String word = matcher.group("word");
            String translatedWord = vocabulary.get(word);
            if(translatedWord == null) {
                translatedWord = vocabulary.get(word.toLowerCase());
            }
            if(translatedWord == null) {
                translatedWord = word;
            }
            matcher.appendReplacement(translatedText, translatedWord);
        }
        matcher.appendTail(translatedText);
        System.out.println(translatedText);
    }

    private void readLines(File file, Consumer<String> consumer) throws IOException {
        try(BufferedReader bufReader = new BufferedReader(new InputStreamReader(
                new FileInputStream(file), TranslatorImpl.CHARSET_NAME))) {
            String readString;
            while ((readString = bufReader.readLine()) != null) {
                consumer.accept(readString);
            }
        }
        catch(UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }

}
