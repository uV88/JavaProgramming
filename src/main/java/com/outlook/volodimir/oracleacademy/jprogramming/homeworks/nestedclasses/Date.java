package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.nestedclasses;

/**
 * Created by volodimir on 26.01.17.
 */
public class Date implements Comparable<Date> {

    public static final DayOfWeek DAY_OF_WEEK_01_01_2001 = DayOfWeek.MONDAY;
    public static final Date DATE_01_01_2001 = new Date(1, 1, 2001);

    private Year year;
    private Month month;
    private Day day;

    /**
     * Creates new Date object
     * @param dayNumber - number of the day of the month, i.e. 1, 2,..., 28, 29, 30, 31
     * @param monthNumber - number of the month, i.e. 1, 2,..., 12
     * @param yearNumber - number of the year, for example 2002
     */
    public Date(int dayNumber, int monthNumber, int yearNumber) {
        this.year = new Year(yearNumber);
        this.month = new Month(monthNumber);
        if(this.month.getDaysCount(this.year.isLeap()) < dayNumber) throw new IllegalArgumentException("Date is not valid");
        this.day = new Day(dayNumber);
    }

    public Year getYear() {
        return this.year;
    }

    public Month getMonth() {
        return this.month;
    }

    public Day getDay() {
        return this.day;
    }

    /**
     * Returns day's number of the year
     * @param date - some date
     * @return number of the day (specified by 'date') of the year, i.e. 1, 2,..., 365 (366)
     */
    public static int getNumOfDayOfYear(Date date) {
        int numOfDaysOfFullMonths = 0;
        for (int i = 1; i < date.month.monthNumber; i++) {
            numOfDaysOfFullMonths += Month.getDaysCountOf(i, date.year.isLeap());
        }
        return numOfDaysOfFullMonths + date.day.dayNumber;
    }

    /**
     * Returns day's number of the year
     * @return number of the day (specified by this object) of the year, i.e. 1, 2,..., 365 (366)
     */
    public  int getNumOfDayOfYear() {
        return Date.getNumOfDayOfYear(this);
    }

    /**
     * Returns count of days
     * @param date - some date
     * @return count of days between 'date' and date of this object  -  if object's date is later than 'date'
     * (-1) * count of days between 'date' and date of this object  -  if object's date is earlier than 'date'
     */
    public int daysCountBetween(Date date) {
        if(this.year.yearNumber == date.year.yearNumber) {
            return this.getNumOfDayOfYear() - date.getNumOfDayOfYear();
        }
        Date earlyDate = date;
        Date lateDate = this;
        int order = 1;
        if(this.compareTo(date) < 0) {
            earlyDate = this;
            lateDate = date;
            order = -1;
        }
        int countOfDaysOfFullYears = 0;
        for (int i = earlyDate.year.yearNumber + 1; i < lateDate.year.yearNumber; i++) {
            countOfDaysOfFullYears += (Year.isLeap(i) ? 366 : 365);
        }
        int countOfDaysOfEarlyYear = (earlyDate.year.isLeap() ? 366 : 365) - earlyDate.getNumOfDayOfYear();
        int countOfDaysOfLateYear = lateDate.getNumOfDayOfYear();
        return order*(countOfDaysOfEarlyYear + countOfDaysOfFullYears + countOfDaysOfLateYear);
    }

    /**
     * Returns day of week
     * @return DayOfWeek object corresponding to the date of this object
     */
    public DayOfWeek getDayOfWeek() {
        int index = ((daysCountBetween(Date.DATE_01_01_2001) % 7) + 7) % 7;
        /* 01.01.2001 is Monday;
        * '(daysCountBetween(Date.DATE_01_01_2001) % 7'  returns
        * index of the day of the week  -  if date of this object is later than 01.01.2001,
        * (index of the day of the week - 7)  -  if date of this object is earlier than 01.01.2001
        */
        return DayOfWeek.valueOf(index);
    }

    @Override
    public int compareTo(Date date) {
        int result = this.year.yearNumber - date.year.yearNumber;
        if(result != 0) return result;
        result = this.month.monthNumber - date.month.monthNumber;
        if(result != 0) return result;
        return this.day.dayNumber - date.day.dayNumber;
    }

    public static class Year {

        private int yearNumber;

        /**
         * Creates new Year object
         * @param yearNumber - number of the year, for example 2002
         */
        public Year(int yearNumber) {
            this.yearNumber = yearNumber;
        }

        public int getYearNumber() {
            return this.yearNumber;
        }

        /**
         * Returns 'true' if year is leap
         * @param year - some year
         * @return 'true' if year is leap, 'false' otherwise
         */
        public static boolean isLeap(Year year) {
            return Year.isLeap(year.yearNumber);
        }

        /**
         * Returns 'true' if year is leap
         * @param yearNumber - number of the year, for example 2010
         * @return 'true' if year is leap, 'false' otherwise
         */
        public static boolean isLeap(int yearNumber) {
            return (yearNumber % 4 == 0 && yearNumber % 100 != 0) || yearNumber % 400 == 0;
        }

        /**
         * Returns 'true' if year is leap
         * @return 'true' if year of this object is leap, 'false' otherwise
         */
        public boolean isLeap() {
            return Year.isLeap(this.yearNumber);
        }
    }

    public static class Month {

        private int monthNumber;

        /**
         * Creates new Month object
         * @param monthNumber - number of the month, i.e. 1, 2,..., 12
         */
        public Month(int monthNumber) {
            if(monthNumber < 1 || monthNumber > 12) throw new IllegalArgumentException("Index value is out of range");
            this.monthNumber = monthNumber;
        }

        public int getMonthNumber() {
            return this.monthNumber;
        }

        /**
         * Returns count of days of the month
         * @param month - some month
         * @param leapYear - 'true', if month belongs to the leap year, 'false' otherwise
         * @return count of days of the month
         */
        public static int getDaysCountOf(Month month, boolean leapYear) {
            return Month.getDaysCountOf(month.monthNumber, leapYear);
        }

        /**
         * Returns count of days of the month
         * @param monthNumber - number of the month, i.e. 1, 2,..., 12
         * @param leapYear - 'true', if month belongs to the leap year, 'false' otherwise
         * @return count of days of the month
         */
        public static int getDaysCountOf(int monthNumber, boolean leapYear) {
            int[] daysCount = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            int monthIndex = monthNumber - 1;
            if(monthIndex == 1 && leapYear) return 29;
            return daysCount[monthIndex];
        }

        /**
         * Returns count of days of the month
         * @param leapYear - 'true', if month of this object belongs to the leap year, 'false' otherwise
         * @return count of days of the month of this object
         */
        public int getDaysCount(boolean leapYear) {
            return Month.getDaysCountOf(this.monthNumber, leapYear);
        }
    }

    public static class Day {

        private int dayNumber;

        /**
         * Creates new Day object
         * @param dayNumber - number of the day of the month, i.e. 1, 2,..., 28, 29, 30, 31
         */
        public Day(int dayNumber) {
            if(dayNumber < 1 || dayNumber > 31) throw new IllegalArgumentException("Index value is out of range");
            this.dayNumber = dayNumber;
        }

        public int getDay() {
            return dayNumber;
        }
    }

    enum DayOfWeek{
        MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATURDAY(5), SUNDAY(6);

        private int index;

        DayOfWeek(int index) {
            this.index = index;
        }

        /**
         * Returns day of week
         * @param index - integer value
         * @return DayOfWeek object corresponding to the index (0 - MONDAY, 1 - TUESDAY,...)
         */
        public static DayOfWeek valueOf(int index) {
            for(DayOfWeek dayOfWeek : DayOfWeek.values()) {
                if(dayOfWeek.index == index) return dayOfWeek;
            }
            throw new IllegalArgumentException("Index value is out of range");
        }
    }

    public static void main(String[] args) {
        Date date = new Date(28, 1, 2017);
        System.out.println("28.01.2017 " + date.getDayOfWeek());
        System.out.println("number of the day of the year: " + date.getNumOfDayOfYear());
        Date earlyDate = new Date(15, 10, 2015);
        System.out.println(" 15.10.2015 - number of the day of the year: " + earlyDate.getNumOfDayOfYear());
        System.out.println("days count between 28.01.2017 and 15.10.2015: " + earlyDate.daysCountBetween(date));
        System.out.println("13.02.1988 " + new Date(13, 2, 1988).getDayOfWeek());
        System.out.println("15.02.1998 " + new Date(15, 2, 1998).getDayOfWeek());
        System.out.println("9.09.2000 " + new Date(9, 9, 2000).getDayOfWeek());
        System.out.println("15.11.2005 " + new Date(15, 11, 2005).getDayOfWeek());
        System.out.println("13.04.2014 " + new Date(13, 4, 2014).getDayOfWeek());
        System.out.println("29.02.2015 " + new Date(29, 2, 2015).getDayOfWeek());
    }
}
