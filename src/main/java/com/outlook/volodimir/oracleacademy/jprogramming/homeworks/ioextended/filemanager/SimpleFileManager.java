package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.ioextended.filemanager;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by volodimir on 24.02.17.
 */
public class SimpleFileManager {

    public static final Pattern COMMAND_PATTERN = Pattern.compile(" *(\\S+) *(\\S*) *(\\S*) *");
    public static final String HOME_DIRECTORY_NAME = "./";
    public static final String PROGRAM_CLOSE_COMMAND = "exit";
    public static final String FILE_LIST_PRINTING_COMMAND = "ls";
    public static final String CHANGE_DIRECTORY_COMMAND = "cd";
    public static final String NEW_FILE_CREATING_COMMAND = "mkf";
    public static final String NEW_DIRECTORY_CREATING_COMMAND = "mkdir";
    public static final String DELETING_FILE_COMMAND = "rm";
    public static final String RENAME_FILE_COMMAND = "rn";

    private File currentDirectory = new File(SimpleFileManager.HOME_DIRECTORY_NAME);

    /**
     * Organizes user-machine interaction to provide process of file management
     * @throws IOException - if an I/O error occurs
     */
    public void takeCommand() throws IOException {
        System.out.println(currentDirectory.getCanonicalPath());
        Scanner scanner = new Scanner(System.in);
        if(scanner.hasNextLine() && scanner.findInLine(SimpleFileManager.COMMAND_PATTERN) != null) {
            String command = scanner.match().group(1);
            String fileName = scanner.match().group(2);
            String newFileName = scanner.match().group(3);
            if(fileName.isEmpty()) {
                switch(command) {
                    case SimpleFileManager.PROGRAM_CLOSE_COMMAND : return;
                    case SimpleFileManager.FILE_LIST_PRINTING_COMMAND : printCurrentDirFiles(); break;
                    default : System.out.println("Can't resolve command");
                }
            }
            else{
                File file = new File(fileName);
                if(newFileName.isEmpty()) {
                    switch (command) {
                        case SimpleFileManager.CHANGE_DIRECTORY_COMMAND:
                            changeCurrentDir(file);
                            break;
                        case SimpleFileManager.NEW_FILE_CREATING_COMMAND:
                            createFile(file);
                            break;
                        case SimpleFileManager.NEW_DIRECTORY_CREATING_COMMAND:
                            createDirectory(file);
                            break;
                        case SimpleFileManager.DELETING_FILE_COMMAND:
                            deleteFile(file);
                            break;
                        default:
                            System.out.println("Can't resolve command");
                    }
                }
                else {
                    if(command.equals(SimpleFileManager.RENAME_FILE_COMMAND)) {
                        renameFile(file, new File(newFileName));
                    }
                    else {
                        System.out.println("Can't resolve command");
                    }
                }
            }
        }
        takeCommand();
    }

    private void printCurrentDirFiles() {
        File[] files = currentDirectory.listFiles();
        if(files != null) {
            for (File file : files) {
                System.out.println((file.isDirectory() ? "dir -> " : "file -> ") + file.getName());
            }
        }
    }

    private void changeCurrentDir(File directory) {
        if(directory.isDirectory()) {
            currentDirectory = directory;
        }
    }

    private void createFile(File file) {
        try {
            if(!file.createNewFile()) {
                System.out.println("Named file already exists!");
            }
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void createDirectory(File directory) {
        if(!directory.mkdir()) {
            System.out.println("Directory wasn't created!");
        }
    }

    private void deleteFile(File file) {
        if(!file.delete()) {
            System.out.println("File or directory wasn't deleted!");
        }
    }

    private void renameFile(File file, File destFile) {
        if(!file.renameTo(destFile)) {
            System.out.println("File or directory wasn't renamed!");
        }
    }

    /**
     * Main method
     * @param args - String parameters
     * @throws IOException - if an I/O error occurs
     */
    public static void main(String[] args) throws IOException {
        SimpleFileManager fileManager = new SimpleFileManager();
        fileManager.takeCommand();
    }
}
