package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

/**
 * Created by volodimir on 08.02.17.
 */
class Entity<T> {

    private final T id;

    /**
     * Creates new Entity object
     * @param id - Entity object identification
     */
    Entity(T id) {
        this.id = id;
    }

    public T getId() {
        return id;
    }
}
