package com.outlook.volodimir.oracleacademy.jprogramming.lessons.junittesting.student;

/**
 * Created by volodimir on 25.01.17.
 */
public class Group {

    private int studiesYear;
    private String department;

    public Group(int studiesYear, String department) {
        this.studiesYear = studiesYear;
        this.department = department;
    }

    public int getStudiesYear() {
        return studiesYear;
    }

    public String getDepartment() {
        return department;
    }
}
