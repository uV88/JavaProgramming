package com.outlook.volodimir.oracleacademy.jprogramming.homeworks.ioextended.filelist;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodimir on 22.02.17.
 */
public class LibraryCards implements FileListExplorer<Book> {

    public static final String CHARSET_NAME = "utf8";
    public static final String SINGLE_BOOK_PATTERN = "(?<author>.+?);(?<title>.+?);(?<year>\\d{1,4})";

    private final File bookCardsFile;
    private final List<Book> bookCards = new ArrayList<>();

    /**
     * Creates new LibraryCards object associated to the book cards file
     * @param bookCardsFile - book cards file
     * @throws IOException - if an I/O error occurs
     */
    public LibraryCards(File bookCardsFile) throws IOException {
        this.bookCardsFile = bookCardsFile;
        refresh();
    }

    /**
     * Creates new LibraryCards object associated to the book cards file
     * @param fileName - name of book cards file
     * @throws IOException - if an I/O error occurs
     */
    public LibraryCards(String fileName) throws IOException {
        this(new File(fileName));
    }

    /**
     * Refreshes content of list of this LibraryCards object according to actual state of book cards file
     * @throws IOException - if an I/O error occurs
     */
    @Override
    public final void refresh() throws IOException {
        bookCards.clear();
        try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(
                new FileInputStream(bookCardsFile), LibraryCards.CHARSET_NAME))) {
            Pattern pattern = Pattern.compile(LibraryCards.SINGLE_BOOK_PATTERN);
            String bookString;
            while ((bookString = bufReader.readLine()) != null) {
                Matcher matcher = pattern.matcher(bookString);
                if (matcher.matches()) {
                    Book bookCard = new Book(matcher.group("title"),
                            matcher.group("author"),
                            Integer.parseInt(matcher.group("year")));
                    bookCards.add(bookCard);
                }
            }
        }
        catch (FileNotFoundException e) {
            /* keep working */
        }
    }

    /**
     * Adds new Book to the list of this LibraryCards object
     * and new appropriate record to the associated book cards file
     * @param bookCard - Book object
     * @throws IOException - if an I/O error occurs
     */
    @Override
    public void addItem(Book bookCard) throws IOException {
        bookCards.add(bookCard);
        try(BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(bookCardsFile, true), LibraryCards.CHARSET_NAME))) {
            bufferedWriter.append(bookCard.getAuthor()).append(';').
                    append(bookCard.getTitle()).append(';').
                    append(Integer.toString(bookCard.getYear()));
            bufferedWriter.newLine();
        }
    }

    /**
     * Returns list reflecting content of associated book cards file
     * at the moment of refresh() method last call
     * (or, if it wasn't call directly, at the moment of this LibraryCards object creating)
     * @return list reflecting content of associated book cards file
     */
    @Override
    public List<Book> exportList() {
        return new ArrayList<>(bookCards);
    }
}
