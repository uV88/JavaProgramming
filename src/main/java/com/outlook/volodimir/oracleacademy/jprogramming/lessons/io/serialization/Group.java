package com.outlook.volodimir.oracleacademy.jprogramming.lessons.io.serialization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by volodimir on 26.02.17.
 */
public class Group implements Serializable {

    private String groupName;
    private String curator;
    private List<Student> students = new ArrayList<>();

    /**
     * Creates new Group object
     * @param groupName - name of group
     * @param curator - curator of the group
     */
    public Group(String groupName, String curator) {
        this.groupName = groupName;
        this.curator = curator;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCurator() {
        return curator;
    }

    public void setCurator(String curator) {
        this.curator = curator;
    }

    public List<Student> getStudents() {
        return students;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupName='" + groupName + '\'' +
                ", curator='" + curator + '\'' +
                ", students=" + students +
                '}';
    }
}
