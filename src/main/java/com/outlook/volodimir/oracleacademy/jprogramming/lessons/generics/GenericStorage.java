package com.outlook.volodimir.oracleacademy.jprogramming.lessons.generics;

/**
 * Created by volodimir on 07.02.17.
 */
public interface GenericStorage<K extends Number, V> {
    /**
     * Adds new key-value pair to this storage
     * @param value - object for adding
     * @return key, associated with object
     */
    K add(V value);

    /**
     * Returns value by its key
     * @param key - key, associated with object
     * @return object according to the key
     */
    V get(K key);

    /**
     * Replaces value by key with new object
     * @param key - key
     * @param value - new object
     * @return object, associated to the key before replacing
     */
    V update(K key, V value);

    /**
     * Deletes key-value pair
     * @param key - key
     * @return object, associated to the key
     */
    V delete(K key);
}
